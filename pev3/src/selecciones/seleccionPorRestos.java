package selecciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;

public class seleccionPorRestos {
	
	public void seleccionRestos(ArrayList<cromosoma> pob){
		// con la puntuacion pi·K y se eligen esas copias de un individuo.
		//Si faltan huecos libres en la nueva pob, se rellenan por ruleta.
		double k = (pob.size())/5; //k es 1/5 de la poblacion elegida
		double[] probs = new double[pob.size()];
		ArrayList<cromosoma> nuevPob = new ArrayList<cromosoma>(pob.size());
		double[] probAcumulada = new double[pob.size()];

		int sumaApt=0;
		for(int i=0; i<pob.size(); i++){
			sumaApt+=pob.get(i).getAptitud();		
			pob.get(i).setPunt_acu(sumaApt);
		}
		double probAcum = 0;
		for(int i=0; i<pob.size(); i++){//Guardamos en el array probs la probabilidad de cada individuo
			probs[i]=1-(pob.get(i).getAptitud()/sumaApt);
			probAcumulada[i]=probs[i] + probAcum;
			probAcum=probAcumulada[i];
		}
		double ncopias;
		int i=0;//pos de pob original
		int n=0;//pos de pob nueva
		while(i<pob.size() && n<pob.size()){
			ncopias = probs[i]*k;
			if(ncopias>=1){
				for(int j=0; j<((int)ncopias)&&(n<pob.size()); j++){
					nuevPob.add(new cromosoma(pob.get(i)));
					n++;
				}
			}
			i++;
		}
		double r=0;
		while(n<pob.size()){//hay que rellenar los huecos libres, si los hay. Por ruleta.
			r = Math.random();
			if(r>=0 && r<=probAcumulada[0]){
				nuevPob.add(new cromosoma(pob.get(0)));
			}else{
				for(int x=1; x<pob.size(); x++){
					if(r<=probAcumulada[x] && r>probAcumulada[x-1]){
						nuevPob.add(new cromosoma(pob.get(x)));
					}
				}
			}
			n++;
		}
		pob = nuevPob;
	}

}

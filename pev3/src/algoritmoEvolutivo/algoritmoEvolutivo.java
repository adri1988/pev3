package algoritmoEvolutivo;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import cruces.crucePorIntercambio;
import edades.*;
import mutaciones.*;
import operacionesArboles.creaArbolRampedAndHalf;
import selecciones.*;
import utils.TArbol;
import utils.bloating;
import utils.elitismo;
import utils.enumCruce;
import utils.enumEdades;
import utils.enumInicializaciones;
import utils.enumMutaciones;
import utils.enumSelecciones;
import org.math.plot.Plot2DPanel;
import org.math.plot.PlotPanel;

import GUI.GUI.SI;
import GUI.GUI.contra;

public class algoritmoEvolutivo {
	
	ArrayList<cromosoma> pob; // poblacion
	int tam_pob=100; // tamanio poblacion
	int num_max_gen=100; // numero maximo de generaciones
	cromosoma elMejor; // mejor individuo
	cromosoma[] elMejorLocal;
	public cromosoma[] getElMejorLocal() {
		return elMejorLocal;
	}

	public void setElMejorLocal(cromosoma[] elMejorLocal) {
		this.elMejorLocal = elMejorLocal;
	}

	int pos_mejor; // posicion del mejor cromosoma
	double prob_cruce=0.8; // probabilidad de cruce
	double prob_mut=0.3; // probabilidad de mutacion	
	int gen_actual=0;	
	ArrayList<cromosoma> elite;
	int elitismo=0;
	boolean contractividad=false;
	boolean escalado;
	int profundidad=0;
	int tipoSeleccion;
	int tipoCruce;
	int tipoMutacion;
	int tipoInicializacion;
	
	
	//Variables auxiliares para las selecciones cruces y mutaciones
	private seleccionPorRuleta sRuleta;
	private seleccionPorTorneo sTorneo;
	private seleccionPorTruncamiento sTrunc;
	private SeleccionUniversalEstocastica sUniversal;
	private seleccionPorRanking sRank;
	private seleccionPorRestos sRestos;
	
	private mutacionesDeArbol mutArbol;
	private mutacionFuncionalSimple mutFuncionalSimple;
	private mutacionTerminalSimple mutTerminalSimple;
	
	private crucePorIntercambio cruceIntercambio;
	
	private double [] generaciones; 	
	private double [] aptitudesDelMejorGlobal; 
	private double [] aptitudesMejoresLocales;
	private double [] mediasDeAptitudes; 
	
	private int alturaMaxima;
	
	private EdadProporcional edadProporcional;
	private EdadLineal edadLineal;
	private EdadBilineal edadBilineal;
		
	
	
	
	
	
	public algoritmoEvolutivo(){}
	
	public void inicializa(int tam_pob2, int num_max_gen2, double prob_cruce2, double prob_mutacion,int hMax,int tipoInit,int tipoCruce,int tipoMutacion,int Seleccion,int elitismo) {
		this.tam_pob=tam_pob2;
		this.num_max_gen=num_max_gen2;		
		this.prob_cruce=prob_cruce2;
		this.prob_mut=prob_mutacion;
		this.tipoInicializacion=tipoInit;
		this.tipoCruce=tipoCruce;
		this.tipoMutacion=tipoMutacion;
		this.tipoSeleccion=Seleccion;
		this.setGeneracionesArray(new double[num_max_gen+1]);		
		this.setAptitudesDelMejorGlobal(new double[num_max_gen+1]);
		this.setAptitudesMejoresLocales(new double[num_max_gen+1]);
		this.setMediasDeAptitudes(new double[num_max_gen+1]);
		this.elitismo=elitismo;
		this.controlBloat=0;
		
		
		elMejor = new cromosoma(pasos); 
		elMejorLocal = new cromosoma[num_max_gen+1];
		pob = new ArrayList<cromosoma>(tam_pob); // creamos el Array de cromosomas	
		
		if (tipoInit==2){//en caso de que sea ramped and half, incializamos de otra manera
			creaArbolRampedAndHalf aRampH = new creaArbolRampedAndHalf();
			aRampH.arbolRampedHalf(pob, hMax,tam_pob,pasos);//aquí creamos los árboles en hMax-1 grupos, cada grupo de creciente o completa
			
		}else{
			
		
		for (int j = 0; j < tam_pob; j++) {
			pob.set(j, new cromosoma(pasos));
			pob.get(j).initCromosoma(hMax,tipoInit);
			
			pob.get(j).setAptitud(pob.get(j).evalua(pasos));			
			}	
		}
		gen_actual=0;
	}
	
	public void inicializa() {
		
		this.setGeneracionesArray(new double[num_max_gen+1]);		
		this.setAptitudesDelMejorGlobal(new double[num_max_gen+1]);
		this.setAptitudesMejoresLocales(new double[num_max_gen+1]);
		this.setMediasDeAptitudes(new double[num_max_gen+1]);	
		sRuleta = new seleccionPorRuleta();
		sTorneo = new seleccionPorTorneo();
		sTrunc  = new seleccionPorTruncamiento();
		sUniversal = new SeleccionUniversalEstocastica();
		sRank = new seleccionPorRanking();
		sRestos = new seleccionPorRestos();
		cruceIntercambio = new crucePorIntercambio();
		mutArbol = new mutacionesDeArbol();
		mutFuncionalSimple = new mutacionFuncionalSimple();
		mutTerminalSimple = new mutacionTerminalSimple();
		//this.controlBloat=0;
		//this.contractividad=false;
		edadProporcional = new EdadProporcional();
		edadLineal= new  EdadLineal();
		edadBilineal = new EdadBilineal();
		
		
		
		elMejor = new cromosoma(pasos); 
		elMejorLocal = new cromosoma[num_max_gen+1];
		pob = new ArrayList<cromosoma>(tam_pob); // creamos el Array de cromosomas	
		//enumInicializaciones.RampedAndHalf ini
		
		if (ini.name().equals("RampedAndHalf")){//en caso de que sea ramped and half, incializamos de otra manera
			System.out.println("RAH");
			creaArbolRampedAndHalf aRampH = new creaArbolRampedAndHalf();
			aRampH.arbolRampedHalf(pob, profundidad,tam_pob,pasos);//aquí creamos los árboles en hMax-1 grupos, cada grupo de creciente o completa
			
		}else{
		
		for (int j = 0; j < tam_pob; j++) {
			pob.add(new cromosoma(pasos));			
			pob.get(j).initCromosoma(this.profundidad,this.tipoInicializacion);	
			//pob.get(j).modificaArbolAmiGusto();
			//System.out.println(pob.get(j).getArbol());
		//	System.out.println(pob.get(j).getArbol().getNumHojas());
		//	printArbolConNumNodos(pob.get(j).arbol);
			pob.get(j).setAptitud(pob.get(j).evalua(pasos));	
		//	System.out.println(pob.get(j).aptitud);
			}	
		}
		gen_actual=0;
	}
	
public void inicializa(int contra) {
		
		this.setGeneracionesArray(new double[num_max_gen+1]);		
		this.setAptitudesDelMejorGlobal(new double[num_max_gen+1]);
		this.setAptitudesMejoresLocales(new double[num_max_gen+1]);
		this.setMediasDeAptitudes(new double[num_max_gen+1]);
		this.contractividad2=contra;
		this.contractividad=true;
		sRuleta = new seleccionPorRuleta();
		sTorneo = new seleccionPorTorneo();
		sTrunc  = new seleccionPorTruncamiento();
		sUniversal = new SeleccionUniversalEstocastica();
		sRank = new seleccionPorRanking();
		sRestos = new seleccionPorRestos();
		cruceIntercambio = new crucePorIntercambio();
		mutArbol = new mutacionesDeArbol();
		mutFuncionalSimple = new mutacionFuncionalSimple();
		mutTerminalSimple = new mutacionTerminalSimple();
		//this.controlBloat=0;
		
		elMejor = new cromosoma(pasos); 
		elMejorLocal = new cromosoma[num_max_gen+1];
		pob = new ArrayList<cromosoma>(tam_pob); // creamos el Array de cromosomas	
		
		if (tipoInicializacion==2){//en caso de que sea ramped and half, incializamos de otra manera
			creaArbolRampedAndHalf aRampH = new creaArbolRampedAndHalf();
			aRampH.arbolRampedHalf(pob, profundidad,tam_pob,pasos);//aquí creamos los árboles en hMax-1 grupos, cada grupo de creciente o completa
			
		}else{
		
		for (int j = 0; j < tam_pob; j++) {
			pob.add(new cromosoma(pasos));			
			pob.get(j).initCromosoma(this.profundidad,this.tipoInicializacion);
		
			pob.get(j).setAptitud(pob.get(j).evalua(pasos));	
		
			}	
		}
		gen_actual=0;
	}
	
	
	private void printArbolConNumNodos(TArbol arbol) {
		
		if (arbol.isEsHoja()){
			System.out.println("Arbol : "+arbol + " Numero de nodos "+ arbol.getNumNodos());
		}
		else{
			if (arbol.getHc()==null){
				System.out.println(arbol.getHi().getNumNodos());
				System.out.println(" Arbol :" + arbol.getHi() + " Numero de nodos "+ arbol.getHi().getNumNodos());
				System.out.println(" Arbol :" + arbol.getHd() + " Numero de nodos "+ arbol.getHd().getNumNodos());
				printArbolConNumNodos(arbol.getHi());
				printArbolConNumNodos(arbol.getHd());
			}
			else{
				System.out.println(" Arbol :" + arbol.getHi() + " Numero de nodos "+ arbol.getHi().getNumNodos());
				System.out.println(" Arbol :" + arbol.getHc() + " Numero de nodos "+ arbol.getHc().getNumNodos());
				System.out.println(" Arbol :" + arbol.getHd() + " Numero de nodos "+ arbol.getHd().getNumNodos());
				
				printArbolConNumNodos(arbol.getHi());
				printArbolConNumNodos(arbol.getHc());
				printArbolConNumNodos(arbol.getHd());
			}
		}
		
		
		
	}

	public JPanel ejecuta(){
		
		
		if (!edad.name().equals("NO"))
			return ejecutaAGConEdades();
		else
			return ejecutaAG();
		
		
	
		
		
	}
	
	private JPanel ejecutaAGConEdades() {		
		
	  
		
		for (int i=0; i<=num_max_gen;i++){ //
			generaciones[i]=i; 
	     }  
		
		evaluarPoblacion();
		calculaEdadPoblacion();	
		mediasDeAptitudes[0]=calculaMediaDePoblacion();
		aptitudesDelMejorGlobal[0]=getElMejor().getAptitud();//Almaceno el mejor de la poblaci�on inicial	
		aptitudesMejoresLocales[0]=getElMejorLocal()[0].getAptitud();
		
		gen_actual++;
		int t=0;
		
		while (gen_actual<num_max_gen && t<=contractividad2){//Algoritmo gen�tico
		
				
			reproduce(tipoCruce);	
			muta(tipoMutacion);		
			evaluarPoblacion();
			
			//  Obtenemos la media de la poblacion hasta la generacion actual, calculamos el mejor global y local
			mediasDeAptitudes[gen_actual]=calculaMediaDePoblacion();
			aptitudesDelMejorGlobal[gen_actual]=getElMejor().getAptitud();//Almaceno el mejor de la generaci�n actual		
			aptitudesMejoresLocales[gen_actual]=getElMejorLocal()[gen_actual].getAptitud();		
			
			calculaEdadPoblacion();
			
			if(contractividad){	
				if (mediasDeAptitudes[gen_actual]>mediasDeAptitudes[gen_actual-1]){
					gen_actual++;
					System.out.println("Ha mejorado la media " + t);
				}
				t++;
			}else{
				gen_actual++;
			}
			
			int tamAntiguo = pob.size();
			eliminaAntiguos();
			int tamNuevo = pob.size();
			System.out.println(tamAntiguo-tamNuevo + " Eliminados");
			
			
		}
		//System.out.println(getElMejor().getArbol());
		//System.out.println(getElMejor().getArbol().getNumNodos());
		//getElMejor().evalua();
		//System.out.println(getElMejor().aptitud);
		JFrame frame = new JFrame("Gráfica");
		Plot2DPanel plot = new Plot2DPanel();		
		plot.addLegend("SOUTH");
	
		plot.addLinePlot("Mejor Global" , generaciones, aptitudesDelMejorGlobal);		
		plot.addLinePlot("Mejor Local", generaciones, aptitudesMejoresLocales);		
		plot.addLinePlot("Media de la Generación", generaciones, mediasDeAptitudes);
		frame.setContentPane(plot);
		return plot;
	}
	
	
	private void eliminaAntiguos() {
		
		for (int i=0;i<pob.size();i++){
			if (pob.get(i).isEdadYaCalculada()){
				if (pob.get(i).getEdad()+2>=this.gen_actual)
					pob.remove(i);//si el elemento ya ha pasado su vida, lo eliminamos
			}
				
		}
		
		
	}

	private void calculaEdadPoblacion() {
		
		
		int aptMin = obtenerAptitudMinima();
		
		for (int i=0;i<pob.size();i++){
			
			switch (edad.name()){
			case "PROPORCIONAL": 
				if (!pob.get(i).isEdadYaCalculada())
					edadProporcional.ejecuta(pob.get(i),(int) mediasDeAptitudes[gen_actual],gen_actual);
				break;
			case "LINEAL":
				if (!pob.get(i).isEdadYaCalculada())
					edadLineal.ejecuta(pob.get(i),aptMin,(int)aptitudesMejoresLocales[gen_actual],gen_actual);
				break;
			case "BILINEAL": 
				if (!pob.get(i).isEdadYaCalculada())
					edadBilineal.ejecuta(pob.get(i),(int) mediasDeAptitudes[gen_actual],aptMin,(int)aptitudesMejoresLocales[gen_actual],gen_actual);
				break;
			default: break;
			
			}
			
		}
		
	}

	private int obtenerAptitudMinima() {
		
		int aptMin = 100;
		
		for (int i=0;i<pob.size();i++){
			if (pob.get(i).getAptitud()<aptMin)
				aptMin = (int) pob.get(i).getAptitud();
		}
		
		return aptMin;
	}

	private void evaluarPoblacionConEdad() {
		
		float punt_acu = 0; // puntuaciÃ³n acumulada
		float aptitud_mejor = 0; // mejor aptitud
		float sumaptitud = 0; // suma de la aptitud	
		elMejorLocal[gen_actual] = new cromosoma(pasos);
		for(int i=0;i<pob.size();i++){
			//System.out.println(pob.get(i).getArbol());
			sumaptitud = sumaptitud + pob.get(i).getAptitud();
			if (pob.get(i).getAptitud() > aptitud_mejor){
				pos_mejor = i;
				aptitud_mejor = pob.get(i).getAptitud();
				elMejorLocal[gen_actual] = new cromosoma(pob.get(i));
			}
		}		 
		//Rellenamos las puntuaciones y puntuaciones acumuladas
		for (int i=0; i< pob.size() ; i++) {

			pob.get(i).setPuntuacion(pob.get(i).getAptitud() /sumaptitud );
			pob.get(i).setPunt_acu( pob.get(i).getPuntuacion() + punt_acu); 
			punt_acu = punt_acu +pob.get(i).getPuntuacion();
		}		

		if(aptitud_mejor>=elMejor.getAptitud())
			elMejor = new cromosoma(pob.get(pos_mejor));
		
	}

	private JPanel ejecutaAG() {
		
		int tamElite=0;
		
		for (int i=0; i<=num_max_gen;i++){ //
			generaciones[i]=i; 
	     }  
		if (elitismo==1){
			 tamElite = (int) (tam_pob*0.02);
		}
		
		
		this.evaluarPoblacion();
			
		mediasDeAptitudes[0]=calculaMediaDePoblacion();
		aptitudesDelMejorGlobal[0]=getElMejor().getAptitud();//Almaceno el mejor de la poblaci�on inicial	
		aptitudesMejoresLocales[0]=getElMejorLocal()[0].getAptitud();
		
		gen_actual++;
		int t=0;
		
		while (gen_actual<num_max_gen && t<=contractividad2){//Algoritmo gen�tico
		System.out.println(gen_actual);
		
		if (gen_actual==120)
			System.out.println(gen_actual);
			
			if(elitismo==1)//Si se ha seleccionado elitismo, separamos a los mejores
				separaMejores(tamElite);
			
			
			if (this.bloat.name().equals("SI"))
				eliminaRamasQUeNuncaSeranEjecutadas();
			//	eliminaIndividuosLargos();
			
			
			
			
			selecciona(tipoSeleccion);
			reproduce(tipoCruce);	
			muta(tipoMutacion);
			
			if(elitismo==1)	//si se ha seleccionado elitismo, volvemos a incluirlos			
				incluyeMejores(tamElite);
			
			
			evaluarPoblacion();
			
			if (this.bloat.name().equals("SI"))
				eliminaRamasQUeNuncaSeranEjecutadas();
			
			//  Obtenemos la media de la poblacion hasta la generacion actual, calculamos el mejor global y local
			mediasDeAptitudes[gen_actual]=calculaMediaDePoblacion();
			aptitudesDelMejorGlobal[gen_actual]=getElMejor().getAptitud();//Almaceno el mejor de la generaci�n actual		
			aptitudesMejoresLocales[gen_actual]=getElMejorLocal()[gen_actual].getAptitud();		
			
			if(contractividad){	
				if (mediasDeAptitudes[gen_actual]>mediasDeAptitudes[gen_actual-1]){
					gen_actual++;
					System.out.println("Ha mejorado la media " + t);
				}
				t++;
			}else{
				gen_actual++;
			}
			if (this.bloat.name().equals("SI"))
				podaArboles(profundidad*10);
			
			
		}
		
		JFrame frame = new JFrame("Gráfica");
		Plot2DPanel plot = new Plot2DPanel();		
		plot.addLegend("SOUTH");
		TArbol aux = utils.utils.copiaArbol(elMejor.getArbol());		
		utils.utils.actualizaArbol(aux, 0);
		elMejor.setArbol(aux);
		elMejor.evalua(pasos);
		
		
		plot.addLinePlot("Mejor Global =" + (int)aptitudesDelMejorGlobal[gen_actual-1] + " " +elMejor.getArbol().getNumHojas(), generaciones, aptitudesDelMejorGlobal);		
		plot.addLinePlot("Mejor Local", generaciones, aptitudesMejoresLocales);		
		plot.addLinePlot("Media de la Generación", generaciones, mediasDeAptitudes);
		frame.setContentPane(plot);
		return plot;
	}

	

	private void eliminaRamasQUeNuncaSeranEjecutadas() {
		
		for (int i=0;i<pob.size();i++){
			pob.get(i).eliminaRamasInutiles(this.pasos);
		}
		
	}

	private void eliminaIndividuosLargos() {
		
	
		for(int i=0;i<(pob.size()-1);i++){//ordenamos los cromosomas por aptitud.(mayor distancia a menor)
            for(int j=i+1;j<pob.size();j++){
                if(pob.get(i).getAptitud()<pob.get(j).getAptitud()){
                    //Intercambiamos valores
                    cromosoma aux=pob.get(i);
                    pob.set(i,pob.get(j));
                    pob.set(j, aux);
                }
            }
        }
		int contador=0; 
		while (contador<pob.size()){
		
			cromosoma aux=pob.get(contador);
			ArrayList<cromosoma> individuosConMismaAptitud = dameListaConIndividuosDeEstaAptitud(aux.getAptitud());
			cromosoma individuoMasPeque = obtenMasPeque(individuosConMismaAptitud);
			int offset=contador;
			for (int j=offset;j<individuosConMismaAptitud.size()+offset;j++){
				pob.set(j, new cromosoma (individuoMasPeque));
				contador++;
			}
			
			
			
		}
		
		
		
		
	}

	private cromosoma obtenMasPeque(ArrayList<cromosoma> individuosConMismaAptitud) {
		cromosoma aux=null;
		
		int longitudMasCorta=Integer.MAX_VALUE;
		for (int i=0;i<individuosConMismaAptitud.size();i++){
			cromosoma auxI=individuosConMismaAptitud.get(i);
			if (auxI.getArbol().getNumNodos() < longitudMasCorta){
				longitudMasCorta=auxI.getArbol().getNumNodos();
				aux=auxI;
			}
		}
		
		
		return  new cromosoma(aux);
	}

	private ArrayList<cromosoma> dameListaConIndividuosDeEstaAptitud(float aptitud) {
		ArrayList<cromosoma> aux = new ArrayList<cromosoma>();
		
		for (int i=0;i<pob.size();i++){
			cromosoma auxI=pob.get(i);
			if (auxI.getAptitud()==aptitud)
				aux.add(auxI);
			
		}
		
		
		return aux;
	}

	private void podaArboles(int profundidad2) {
		
		for (int i=0;i<pob.size();i++){
		
			utils.utils.podaArbolHastaProfundidad(pob.get(i).getArbol(),profundidad2,0);			
			utils.utils.actualizaArbol(pob.get(i).getArbol(), 0);
			
		}
		
	}

	public void evaluarPoblacion(){//Evaluamos la población buscando el mínimo siempre
		float punt_acu = 0; // puntuaciÃ³n acumulada
		float aptitud_mejor = 0; // mejor aptitud
		float sumaptitud = 0; // suma de la aptitud	
		elMejorLocal[gen_actual] = new cromosoma(pasos);
		for(int i=0;i<pob.size();i++){
			//System.out.println(pob.get(i).getArbol());
			sumaptitud = sumaptitud + pob.get(i).getAptitud();
			if (pob.get(i).getAptitud() > aptitud_mejor){
				pos_mejor = i;
				aptitud_mejor = pob.get(i).getAptitud();
				elMejorLocal[gen_actual] = new cromosoma(pob.get(i));
			}
		}		 
		//Rellenamos las puntuaciones y puntuaciones acumuladas
		for (int i=0; i< pob.size() ; i++) {

			pob.get(i).setPuntuacion(pob.get(i).getAptitud() /sumaptitud );
			pob.get(i).setPunt_acu( pob.get(i).getPuntuacion() + punt_acu); 
			punt_acu = punt_acu +pob.get(i).getPuntuacion();
		}		

		if(aptitud_mejor>=elMejor.getAptitud())
			elMejor = new cromosoma(pob.get(pos_mejor));

	}	
	
	
	
	
	

	public void selecciona(int tipoSeleccion){
		/*1- ruleta
		 * 2- torneo
		 * 3- truncamiento
		 * 4- universal
		 * */
		
		switch (selec.name()){
		case "Ruleta": sRuleta.ejecuta(pob);
				break;
				
		case "Torneo": sTorneo.ejecuta(pob,1);
				break;	
				
		case "Truncamiento": sTrunc.ejecuta(pob);
		 		break;
		 		
		case "Estocastico": sUniversal.ejecuta(pob);
				break;
		case "Ranking": sRank.seleccionRanking(pob);
				break;
		
		case "Restos": sRestos.seleccionRestos(pob);
				break;
				
		default:break;
		}
	}
	
	public void muta(int tipoMutacion){
		
		/*0- deArbol
		 * 1- funcional simple
		 * 2- terminal simple		 *
		 * */
	
		switch (mut.name()){
		case "TerminalSimple":  mutTerminalSimple.ejecuta(pob,prob_mut,pasos);
			//	System.out.println("mutTerminalSimple");
				break;
				
		case "FuncionalSimple": mutFuncionalSimple.ejecuta(pob,prob_mut,pasos);	//System.out.println("mutFuncionalSimple");
				break;
				
		case "DeArbol":mutArbol.ejecuta(pob,prob_mut,profundidad,pasos);//System.out.println("mutArbol");
				break;
				
		default:break;
		}
		
	}
	
	public void reproduce(int tipoCruce){
		
		/*
		 * Aquí implementamos los distintos tipos de reproducción
		 *  
		 *  tipo = 1 -> cruce por intercambio
		 *   *  
		 * */

		//seleccionados para reproducir
		int[] sel_cruce = new int[pob.size()];
		//contador seleccionados
		int num_sel_cruce = 0;		
		double prob;

		//Se eligen los individuos a cruzar
		for (int i=0;i<pob.size();i++){
			//se generan tam_pob nÃºmeros aleatorios en [0 1)
			prob = 	Math.random();
			//se eligen los individuos de las posiciones i si prob < prob_cruce
			if (prob < prob_cruce ){
				sel_cruce[num_sel_cruce] = i;
				num_sel_cruce++;
			}
		}

		// el numero de seleccionados se hace par

		if ((num_sel_cruce%2)==1)
			num_sel_cruce--;			

		// se cruzan los individuos elegidos en un punto al azar

		for (int i=0;i<num_sel_cruce; i=i+2){
			cromosoma hijo1 = new cromosoma(pob.get(sel_cruce[i]));
			cromosoma hijo2 = new cromosoma(pob.get(sel_cruce[i+1]));
			cromosoma mejorHijo1,mejorHijo2;
			mejorHijo1 = new cromosoma(hijo1);
			mejorHijo2 = new cromosoma(hijo2);
			
			switch (tipoCruce){
			case 0:			
				cruceIntercambio.ejecuta(pob.get(sel_cruce[i]), pob.get(sel_cruce[i+1]),hijo1, hijo2,pasos);//ejecuta(pob.get(sel_cruce[i])], pob.get(sel_cruce[i]),hijo1, hijo2);
				
				
				boolean peores = true;
				int contador =0;
				
			
				mejorHijo1 = new cromosoma (hijo1);
				mejorHijo2 = new cromosoma (hijo2);
				
				boolean hijo1Mejor=false;
				boolean hijo2Mejor=false;
				
				int aptitudPadre1 = (int) pob.get(sel_cruce[i]).getAptitud();
				int aptitudPadre2 = (int) pob.get(sel_cruce[i+1]).getAptitud();
				
				int aptitudHijo1 = (int) hijo1.getAptitud();
				int aptitudHijo2 = (int) hijo2.getAptitud();
			
				while (peores && contador < 15){
					
					
					
					
					if (aptitudHijo1 > Math.max(aptitudPadre1, aptitudPadre2)){
						hijo1Mejor=true;
					}
					if (aptitudHijo2 > Math.max(aptitudPadre1, aptitudPadre2)){
						hijo2Mejor=true;
					}
					
					if (hijo1Mejor && hijo2Mejor){
						peores=false;
						hijo1Mejor=false;
						hijo2Mejor=false;
						//mejorHijo1= new cromosoma(hijo1);
						//mejorHijo2= new cromosoma(hijo2);
					}			
					else{
						hijo1 = new cromosoma(pob.get(sel_cruce[i]));
						hijo2 = new cromosoma(pob.get(sel_cruce[i+1]));	
						//System.out.println("entrado en zona critica");
						if (hijo1Mejor)			//si el hijo1 YA es mejor que los padres, solo se ejecuta el cruce para el hijo2				
							cruceIntercambio.ejecutaSoloUnHijo(pob.get(sel_cruce[i]),pob.get(sel_cruce[i+1]),hijo2, pasos,2);
						if (hijo2Mejor)			//si el hijo2 YA es mejor que los padres, solo se ejecuta el cruce para el hijo1		
							cruceIntercambio.ejecutaSoloUnHijo(pob.get(sel_cruce[i]),pob.get(sel_cruce[i+1]),hijo1, pasos,1);
						if (!hijo2Mejor && !hijo1Mejor) //si ningun hijo es mejor que los padres, se aplica el cruce a ambos hijos		
							cruceIntercambio.ejecuta(pob.get(sel_cruce[i]), pob.get(sel_cruce[i+1]),hijo1,hijo2, pasos);
						aptitudHijo1 = (int) hijo1.getAptitud();
						aptitudHijo2 = (int) hijo2.getAptitud();
					//	System.out.println("saliendo de zona critica");
						
						contador++;
						if (!hijo1Mejor && aptitudHijo1>mejorHijo1.getAptitud())
							mejorHijo1 = new cromosoma(hijo1);
						if (!hijo2Mejor &&aptitudHijo2>mejorHijo2.getAptitud())
							mejorHijo2 = new cromosoma(hijo2);
						
					}
					
					
				}
				hijo1Mejor=false;			
				hijo2Mejor=false;	
				
				
				
				break;	
			default: 
				break;
			}
			if (!edad.name().equals("NO")){ // si tenemops control de edades, no reemplazamos a los padres por sus hijos
				pob.add(mejorHijo1);
				pob.add(mejorHijo2);
			}
			
			else{
				
							
				pob.set(sel_cruce[i], mejorHijo1);
				pob.set(sel_cruce[i+1], mejorHijo2);
				
				
			}
		//	System.out.println(i);
		}	
		
	
		
	}
	
	
	//Elitismo
		public void separaMejores(int tamElite) {
			ArrayList<cromosoma> auxiliar = new ArrayList<cromosoma>();
			elite = new ArrayList<cromosoma>();
			for (int i=0;i<tam_pob;i++)
				auxiliar.add(pob.get(i));
			for (int j=0;j<tamElite;j++){
				int mejor = sacaIndiceDelMejor(auxiliar);
				elite.add(auxiliar.get(mejor));
				auxiliar.remove(mejor);
				tam_pob--;
			}
			pob=auxiliar;
		}

		private int sacaIndiceDelMejor(ArrayList<cromosoma> auxiliar) {
			double mejor=auxiliar.get(0).getAptitud();
			int indiceMejor=0;

			for (int i=1;i<tam_pob;i++)			
				if (auxiliar.get(i).getAptitud()>mejor){
					mejor=auxiliar.get(i).getAptitud();
					indiceMejor=i;	
				}			
			return indiceMejor;
		}

		public void incluyeMejores(int tamElite) {
			ArrayList<cromosoma> auxiliar = new 	ArrayList<cromosoma>(tam_pob+tamElite);
			for (int i=0;i<tam_pob;i++){
				auxiliar.add(new cromosoma(pob.get(i)));
			}
			for (int i=tam_pob;i<tamElite+tam_pob;i++){
				auxiliar.add(new cromosoma(new cromosoma(elite.get(i-tam_pob))));
			
			}
			pob = auxiliar;
			tam_pob = tam_pob+tamElite;
		}
		
		
		private double calculaMediaDePoblacion() {
			
			
			double media =0;
			for (int i=0;i<pob.size();i++){
				media+=pob.get(i).getAptitud();
			}
			media = media/pob.size();
			return media;
		}
	
	public int gettam_pob() {
		return tam_pob;
	}
	
	public void settam_pob(int tam_pob) {
		this.tam_pob = tam_pob;
	}

	public cromosoma getElMejor() {
		return elMejor;
	}

	public void setElMejor(cromosoma elMejor) {
		this.elMejor = elMejor;
	}

	

	public boolean isElitismo() {
		return elitismo==1;
	}
	
	public elitismo getElitismo() {
		if (elitismo==1)
			return utils.elitismo.SI;		
		
		return utils.elitismo.NO;
	}

	public void setElitismo(elitismo elit) {
		if (elit.name().equals(utils.elitismo.SI.toString()))		
			this.elitismo = 1;
		else
			this.elitismo = 0;
	}	
	

	public double getProb_cruce() {
		return prob_cruce;
	}

	public void setProb_cruce(double prob_cruce) {
		this.prob_cruce = prob_cruce;
	}

	public double getProb_mut() {
		return prob_mut;
	}

	public void setProb_mut(double prob_mut) {
		this.prob_mut = prob_mut;
	}

	public int getProfundidad() {
		return profundidad;
	}

	public void setProfundidad(int profundidad) {
		this.profundidad = profundidad;
	}

	public int getTipoSeleccion() {
		return tipoSeleccion;
	}

	public void setTipoSeleccion(int tipoSeleccion) {
		this.tipoSeleccion = tipoSeleccion;
	}

	public int getTipoCruce1() {
		return tipoCruce;
	}

	public void setTipoCruce1(int tipoCruce) {
		this.tipoCruce = tipoCruce;
	}
	
	

	public int getTipoMutacion() {
		return tipoMutacion;
	}

	public void setTipoMutacion(int tipoMutacion) {
		this.tipoMutacion = tipoMutacion;
	}

	public int getTipoInicializacion() {
		return tipoInicializacion;
	}

	public void setTipoInicializacion(int tipoInicializacion) {
		this.tipoInicializacion = tipoInicializacion;
	}

	public seleccionPorRuleta getsRuleta() {
		return sRuleta;
	}

	public void setsRuleta(seleccionPorRuleta sRuleta) {
		this.sRuleta = sRuleta;
	}

	public seleccionPorTorneo getsTorneo() {
		return sTorneo;
	}

	public void setsTorneo(seleccionPorTorneo sTorneo) {
		this.sTorneo = sTorneo;
	}

	public seleccionPorTruncamiento getsTrunc() {
		return sTrunc;
	}

	public void setsTrunc(seleccionPorTruncamiento sTrunc) {
		this.sTrunc = sTrunc;
	}

	public SeleccionUniversalEstocastica getsUniversal() {
		return sUniversal;
	}

	public void setsUniversal(SeleccionUniversalEstocastica sUniversal) {
		this.sUniversal = sUniversal;
	}

	public double [] getGeneracionesArray() {
		return generaciones;
	}

	public void setGeneracionesArray(double [] generaciones) {
		this.generaciones = generaciones;
	}

	public double [] getAptitudesDelMejorGlobal() {
		return aptitudesDelMejorGlobal;
	}

	public void setAptitudesDelMejorGlobal(double [] aptitudesDelMejorGlobal) {
		this.aptitudesDelMejorGlobal = aptitudesDelMejorGlobal;
	}

	public double [] getAptitudesMejoresLocales() {
		return aptitudesMejoresLocales;
	}

	public void setAptitudesMejoresLocales(double [] aptitudesMejoresLocales) {
		this.aptitudesMejoresLocales = aptitudesMejoresLocales;
	}

	public double [] getMediasDeAptitudes() {
		return mediasDeAptitudes;
	}

	public void setMediasDeAptitudes(double [] mediasDeAptitudes) {
		this.mediasDeAptitudes = mediasDeAptitudes;
	}
	
	public void setNum_max_gen(int generaciones) {
		this.num_max_gen = generaciones;
	}

	public int getNum_max_gen() {
		return this.num_max_gen;
	}
	
	
	
	
	
	
	
	
	public void setIncializacion(String incializacion) {
		this.tipoInicializacion = incializacionDeStringAint(incializacion);
		
	}
	
	

	

	
	
	private int incializacionDeStringAint(String incializacion) {
		
		switch (incializacion){
		case "Completa": return 0;
			
		case "Creciente":return 1;
			
		case "RampedAndHalf":return 2;
			
		default: break;
	}
		
		return 0;
	}

	private int seleccionDeStringAint(String seleccion) {
		switch (seleccion){
		case "Ruleta": return 0;
			
		case "Torneo":return 1;
			
		case "Truncamiento":return 2;
		
		case "Estocastico":return 3;
		
		case "Ranking":return 4;
		
		case "Restos":return 5;
		
		
			
		default: break;
	}
		return 0;
	}	
	
	private int mutacionDeStringAint(String mutacion) {
		switch (mutacion){
		case "DeArbol": return 2;
			
		case "FuncionalSimple":return 1;
			
		case "TerminalSimple":return 0;
			
		default: break;
	}
		return -1;
		
	
	}
	private elitismo elit= utils.elitismo.NO; 
	public elitismo getElit() { return elit; }
	
	
	
	
	public void setElit(elitismo elit) {	
		
		this.elit = elit;	
		
		if (elit.name().equals("SI"))
			this.elitismo=1;
		else
			this.elitismo=0;	
	}
	
	private contra cont = new GUI.GUI.NO() ;
	public contra getContra() { return cont; }
	private int contractividad2;
	
	
	
	public void setContra(contra cont) {	
		
		this.cont = cont;	
		if (cont.getClass()==SI.class){
			SI s = (SI) cont;
			this.contractividad2=s.getNumMaxIter();
			System.out.println("SI");
			System.out.println(contractividad2);			
			this.contractividad=true;
			
		}
			
		else{
			this.contractividad2=0;
			this.contractividad=false;
			System.out.println("NO");
		}
			
	
		if (bloat.name().equals("SI"))
			this.controlBloat=1;
		else
			this.controlBloat=0;	
	}
	
	
	private bloating bloat= utils.bloating.SI; 
	public bloating getBloat() { return bloat; }
	private int controlBloat;
	
	
	
	public void setBloat(bloating bloat) {	
		
		this.bloat = bloat;	
	
		if (bloat.name().equals("SI"))
			this.controlBloat=1;
		else
			this.controlBloat=0;	
	}
	
	private enumEdades edad= enumEdades.NO;
	public enumEdades getEdad() { return edad; }
	//private int controlBloat;
	
	
	
	public void setEdad(enumEdades edad) {	
		
		this.edad = edad;	
	}
	
	
	
	
	
	private enumSelecciones selec = enumSelecciones.Ranking; 
	
	public void setSeleccion(enumSelecciones seleccion) {
		this.selec=seleccion;
		this.tipoSeleccion = seleccionDeStringAint(seleccion.name());
		
	}	

	public enumSelecciones getSeleccion() {
		return selec;
		//return seleccionDeintAString(tipoSeleccion);
	}
	
	private enumCruce cru = enumCruce.crucePorIntercambio;
	
	public enumCruce getTipoCruce() {
		return cru;
	}

	public void setTipoCruce(enumCruce cru) {
		this.cru = cru;
		this.tipoCruce = 1;//mientras no tenga mas cruces lo dejo asi
		
		
	}
	
	private enumMutaciones mut = enumMutaciones.DeArbol;
	
	public void setMutacion(enumMutaciones mutacion) {
		this.mut = mutacion;
		System.out.println(mutacion.name());
		this.tipoMutacion= mutacionDeStringAint(mutacion.name());
		
	}		

	public enumMutaciones getMutacion() {
		return this.mut;
	}
	
    private enumInicializaciones ini = enumInicializaciones.RampedAndHalf;
	
	public void setIncializacion(enumInicializaciones ini) {
		this.ini = ini;
		this.tipoInicializacion= incializacionDeStringAint(ini.name());
		
	}		

	public enumInicializaciones getIncializacion() {
		return this.ini;
	}
	
	private int pasos = 400;
	public int getPasos (){
		return pasos;
	}
	
	public void setPasos(int pasos){
		this.pasos=pasos;
	}
	
	
	
	
	

}

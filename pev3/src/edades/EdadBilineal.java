package edades;

import algoritmoEvolutivo.cromosoma;
import utils.utils;

public class EdadBilineal {

	public void ejecuta(cromosoma c, int aptMed , int aptMin, int aptMax, int gen_actual) {
		
		int edad = 0;
		
		if (c.getAptitud()<=aptMed)
			edad = (int) (utils.minTV + utils.diftTV * ( (c.getAptitud() - aptMin) / (aptMed - aptMin) ));
		else
			edad=(int) (  (utils.minTV + utils.maxTV)/2 + utils.diftTV* (   (c.getAptitud() - aptMed) / (aptMax - aptMed)              )  );  
			
		c.setEdad(edad+gen_actual);	
		c.setEdadYaCalculada(true);
		
	}

}

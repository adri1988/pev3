package selecciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;


public class seleccionPorRuleta {

	public void ejecuta(ArrayList<cromosoma> pob) {
		
		int[] sel_super = new int[pob.size()];//seleccionados para sobrevivir
		double prob; // probabilidad de seleccion
		int pos_super; // posicion del superviviente
		ArrayList<cromosoma> nueva_pob= new ArrayList<cromosoma>(pob.size()); 
		for (int i=0;i<pob.size();i++){
			prob = 	Math.random();
			pos_super=0;
			while ((pos_super<pob.size()) && (prob>pob.get(pos_super).getPunt_acu())){
				sel_super[i] = pos_super;
				pos_super++;
			}
		}		
		for (int i=0;i<pob.size();i++){//genero la población intermedia
			//nueva_pob[i] =  new cromosomaFuncionGenerica(pob[sel_super[i]]);
			nueva_pob.add(new cromosoma(pob.get(sel_super[i])));
		}
		
	
		
		pob = nueva_pob;//actualizo la población
		
	}

}

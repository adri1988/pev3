package selecciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;


public class seleccionPorTorneo {

	public void ejecuta(ArrayList<cromosoma> pob,int bloating) {
		int[] sel_super = new int[pob.size()];//seleccionados para sobrevivir
		ArrayList<cromosoma> nueva_pob= new ArrayList<cromosoma>(pob.size()); 
		for(int i=0;i<pob.size();i++){
		//tengo que obtener una muestra de tamaño 2 y asegurarme que no son iguales
		int muestra1 = randomWithRange(0,pob.size()-1); // probabilidad de seleccion
		int muestra2 = randomWithRange(0,pob.size()-1); // probabilidad de seleccion
		while (muestra1==muestra2){
			 muestra1 = randomWithRange(0,pob.size()-1); // probabilidad de seleccion
			 muestra2 = randomWithRange(0,pob.size()-1); // probabilidad de seleccion
		}
		//Al salir del bucle me aseguro que muestra1!=Muestra2
		
		//Obtengo el fitness de cada muestra de la población
		double muestra1Fitness = pob.get(muestra1).getAptitud();
		double muestra2Fitness = pob.get(muestra2).getAptitud();
		int pos_super;// posición del superviviente
		
			if (bloating==1){
			
				if (muestra1Fitness==muestra2Fitness){
					
					if (pob.get(muestra1).getArbol().getNumNodos()>pob.get(muestra2).getArbol().getNumNodos())
						pos_super=muestra2;
					else
						pos_super=muestra1;
				
				}
				else{
			
					if (muestra1Fitness>muestra2Fitness){
						pos_super=muestra1;//es mejor la muestra 1
					}
					else{
						pos_super=muestra2;//es mejor la muestra 2
					}
				}
			}
			else{
				if (muestra1Fitness>muestra2Fitness){
					pos_super=muestra1;//es mejor la muestra 1
				}
				else{
					pos_super=muestra2;//es mejor la muestra 2
				}
			}
		nueva_pob.add( new cromosoma(pob.get(sel_super[pos_super])));//ojo aqui
		}
		
		
	
		pob = nueva_pob;//actualizo la población
		
	}
	
	int randomWithRange(int min, int max){
		   int range = (max - min) + 1;     
		   return (int)(Math.random() * range) + min;
		}

}

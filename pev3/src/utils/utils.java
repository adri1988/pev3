package utils;

import operacionesArboles.creaArbolCompleta;

public class utils {
	
	public static final String[][] MAPASANTAFE = {
			
			{"@","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","#","#","0","0","0","0"},
			{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","#","0","0"},
			{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","#","0","0"},
			{"0","0","0","#","#","#","#","0","#","#","#","#","#","0","0","0","0","0","0","0","0","#","#","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","#","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","#","#","#","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","#","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0"},
			{"0","0","0","#","#","0","0","#","#","#","#","#","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","#","0","0","0","0","0","0","#","#","#","#","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","#","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","#","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
			{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"}
			};
	public static final int minTV = 1;
	public static final int maxTV = 7;
	public static final int diftTV = (maxTV - minTV)/2;
	
	
	public static String operando_aleatorio() {
		int operando = randomWithRange(0,1);
		return Terminales.values()[operando].toString();
		
	}
	public static int generaAleatorioEntre0y1() {
		return randomWithRange(0,1);
		
	}
	public static String operador_aleatorio() {
		int operador = randomWithRange(0,1);
		double prob = Math.random();
		
		if (prob<0.05){
			return "SIC";
		}
		
		return Funciones.values()[operador].toString();

	}

	public static int randomWithRange(int min, int max){
		   int range = (max - min) + 1;     
		   return (int)(Math.random() * range) + min;
		}
	
	public static TArbol copiaArbol(TArbol arbol) {
		
		TArbol copia = clonaArbol (arbol);	
		//System.out.println(arbol);
		//System.out.println(copia);
		return copia;
	}
	private static TArbol clonaArbol(TArbol arbol) {
		
		TArbol p = new TArbol();
		//System.out.println(arbol);
		if (!arbol.isEsHoja()){
		if (arbol.getHc()==null){
			p.setHi(clonaArbol(arbol.getHi()));
			p.setHd(clonaArbol(arbol.getHd()));
			
			
		}
		else {
			p.setHi(clonaArbol(arbol.getHi()));
			p.setHc(clonaArbol(arbol.getHc()));
			p.setHd(clonaArbol(arbol.getHd()));
			
			
		}
		}
		
		p.setValor(arbol.getValor());
		p.setEsHoja(arbol.isEsHoja());
		p.setNumHojas(arbol.getNumHojas());
		p.setNumNodos(arbol.getNumNodos());
		p.setEsHc(arbol.isEsHc());
		p.setEsHd(arbol.isEsHd());
		p.setEsHi(arbol.isEsHi());
		p.setPadre(arbol.getPadre());
		p.setProfundidad(arbol.getProfundidad());	
		return p;
	}
	
    public static void sustituirSubarbol(TArbol arbol,int nodo_cruce, TArbol subarbol2) {
		
		
		if (nodo_cruce==0){
			arbol.setValoresArbol(subarbol2);
		}
		else{		
		
		 recorrePreOrdenParaSubstitucion(arbol,nodo_cruce,0,subarbol2);
		}
		
	}
	
	
	private static TArbol recorrePreOrdenParaSubstitucion(TArbol tArbol, int nodo_cruce, int nodoActual, TArbol subarbol2) {

		if (nodo_cruce==nodoActual){//Si el nodo a intercambiar es uno de los hijos
			
			
			return utils.copiaArbol(subarbol2);
		
			
		}
		
		else{
			
			if (tArbol.getValor().equals( Funciones.PROGN3.toString())){
				if (tArbol.getHi().getNumNodos()+nodoActual>=nodo_cruce){			
					//Actualizo valores de hijo y nodo
					tArbol.setHi(recorrePreOrdenParaSubstitucion(tArbol.getHi(),nodo_cruce,nodoActual+1,subarbol2));
					tArbol.setNumNodos(tArbol.getHi().getNumNodos()+tArbol.getHd().getNumNodos()+tArbol.getHc().getNumNodos()+1);
					tArbol.getHi().setPadre(tArbol);
					tArbol.setProfundidad(nodoActual);
					tArbol.setNumHojas(tArbol.getHi().getNumHojas()+tArbol.getHd().getNumHojas()+tArbol.getHc().getNumHojas());
					
					return tArbol;
					
					
				}
					
				if (tArbol.getHc().getNumNodos() + tArbol.getHi().getNumNodos() +nodoActual>=nodo_cruce){
					//Actualizo valores de hijo y nodo
					tArbol.setHc(recorrePreOrdenParaSubstitucion(tArbol.getHc(),nodo_cruce,tArbol.getHi().getNumNodos()+nodoActual+1,subarbol2));
					tArbol.setNumNodos(tArbol.getHi().getNumNodos()+tArbol.getHd().getNumNodos()+tArbol.getHc().getNumNodos()+1);
					tArbol.getHc().setPadre(tArbol);
					tArbol.setProfundidad(nodoActual);
					tArbol.setNumHojas(tArbol.getHi().getNumHojas()+tArbol.getHd().getNumHojas()+tArbol.getHc().getNumHojas());
					
					
					return tArbol;
					
				}	
				//Actualizo valores de hijo y nodo
				tArbol.setHd(recorrePreOrdenParaSubstitucion(tArbol.getHd(),nodo_cruce,tArbol.getHi().getNumNodos()+tArbol.getHc().getNumNodos()+nodoActual+1,subarbol2));
				tArbol.setNumNodos(tArbol.getHi().getNumNodos()+tArbol.getHd().getNumNodos()+tArbol.getHc().getNumNodos()+1);
				
				
				tArbol.getHd().setPadre(tArbol);
				tArbol.setProfundidad(nodoActual);
				tArbol.setNumHojas(tArbol.getHi().getNumHojas()+tArbol.getHd().getNumHojas()+tArbol.getHc().getNumHojas());
				
				
				return tArbol;
				
			}
			else{
				if (tArbol.getHi().getNumNodos()+nodoActual>=nodo_cruce){
					//Actualizo valores de hijo y nodo
					tArbol.setHi(recorrePreOrdenParaSubstitucion(tArbol.getHi(),nodo_cruce,nodoActual+1,subarbol2));
					tArbol.setNumNodos(tArbol.getHi().getNumNodos()+tArbol.getHd().getNumNodos()+1);
					
					tArbol.getHi().setPadre(tArbol);
					tArbol.setProfundidad(nodoActual);
					tArbol.setNumHojas(tArbol.getHi().getNumHojas()+tArbol.getHd().getNumHojas());
					
					return tArbol;
				
				}
				
				//Actualizo valores de hijo y nodo
				tArbol.setHd(recorrePreOrdenParaSubstitucion(tArbol.getHd(),nodo_cruce,tArbol.getHi().getNumNodos()+nodoActual+1,subarbol2));
				tArbol.setNumNodos(tArbol.getHi().getNumNodos()+tArbol.getHd().getNumNodos()+1);
				tArbol.getHd().setPadre(tArbol);
				tArbol.setProfundidad(nodoActual);
				tArbol.setNumHojas(tArbol.getHi().getNumHojas()+tArbol.getHd().getNumHojas());
				
				
				return tArbol;
				
			}
			
		}
		
			
		
	}
	public static void actualizaArbol(TArbol arbol,int profundidad) {
		
		if (profundidad==0)
			arbol.setEsRaiz(true);
		if (arbol.getValor().equals("AVANZA")  || arbol.getValor().equals("GIRA_DERECHA") || arbol.getValor().equals("GIRA_IZQUIERDA") ){
			arbol.setEsHoja(true);
			arbol.setNumNodos(1);
			arbol.setNumHojas(1);
			arbol.setHc(null);
			arbol.setHd(null);
			arbol.setHi(null);
			arbol.setProfundidad(profundidad);			
		}
		else{
			if (arbol.getHc()==null){
				actualizaArbol(arbol.getHi(),profundidad+1);
				actualizaArbol(arbol.getHd(),profundidad+1);
				arbol.setNumHojas(arbol.getHi().getNumHojas()+arbol.getHd().getNumHojas());
				arbol.setNumNodos(arbol.getHi().getNumNodos()+arbol.getHd().getNumNodos()+1);
				arbol.setProfundidad(profundidad);	
				arbol.getHi().setPadre(arbol);
				arbol.getHd().setPadre(arbol);
			}
			else{
				actualizaArbol(arbol.getHi(),profundidad+1);
				actualizaArbol(arbol.getHc(),profundidad+1);
				actualizaArbol(arbol.getHd(),profundidad+1);
				arbol.setNumHojas(arbol.getHi().getNumHojas()+arbol.getHd().getNumHojas()+arbol.getHc().getNumHojas());
				arbol.setNumNodos(arbol.getHi().getNumNodos()+arbol.getHd().getNumNodos()+arbol.getHc().getNumNodos()+1);
				arbol.setProfundidad(profundidad);	
				arbol.getHi().setPadre(arbol);
				arbol.getHd().setPadre(arbol);
				arbol.getHc().setPadre(arbol);
			}
		}
		
		
	}
	public static void podaArbolHastaProfundidad(TArbol arbol, int profundidadMaxima,int profundidadActual) {
		
		if (profundidadActual == profundidadMaxima){
			//si hemos alcanzado la profundidad máxima debemos asignar un terminal aleatorio
			double num =Math.random();
			if (num<0.3){				
				arbol.setValor("AVANZA");
				arbol.setNumNodos(1);	
				arbol.setEsHoja(true);
				arbol.setNumHojas(1);
				arbol.setProfundidad(profundidadActual);
			}
			else{
				String operando = utils.operando_aleatorio();
				arbol.setValor(operando);
				arbol.setNumNodos(1);	
				arbol.setEsHoja(true);
				arbol.setNumHojas(1);
				arbol.setProfundidad(profundidadActual);
			}
			
		}
		else
		{
			
			if (!arbol.isEsHoja()){
			
			
				if (arbol.getHc()==null){
					podaArbolHastaProfundidad(arbol.getHi(),profundidadMaxima,profundidadActual+1);
					podaArbolHastaProfundidad(arbol.getHd(),profundidadMaxima,profundidadActual+1);
					arbol.setNumHojas(arbol.getHi().getNumHojas()+arbol.getHd().getNumHojas());
					arbol.setNumNodos(arbol.getHi().getNumNodos()+arbol.getHd().getNumNodos()+1);
					arbol.setProfundidad(profundidadActual);
					
					
				}
				else{
					podaArbolHastaProfundidad(arbol.getHi(),profundidadMaxima,profundidadActual+1);
					podaArbolHastaProfundidad(arbol.getHc(),profundidadMaxima,profundidadActual+1);
					podaArbolHastaProfundidad(arbol.getHd(),profundidadMaxima,profundidadActual+1);
					
					arbol.setNumHojas(arbol.getHi().getNumHojas()+arbol.getHd().getNumHojas()+arbol.getHc().getNumHojas());
					arbol.setNumNodos(arbol.getHi().getNumNodos()+arbol.getHd().getNumNodos()+arbol.getHc().getNumNodos()+1);
					arbol.setProfundidad(profundidadActual);
				}
			}
				
			
			
		}

		
	}
	public static void eliminaRamasInutiles(TArbol arbol, int pasos) {
		
		if (arbol.isEsHoja())
			return;//no se elimina nada
		else{
			
			if (arbol.getValor().equals("PROGN3")){
				
				int hojasHi = arbol.getHi().getNumHojas();
				if (hojasHi>pasos){
					creaArbolCompleta aCom = new creaArbolCompleta();
					TArbol aux = new TArbol();
					aCom.arbolCompleta(aux, 0, 0);//creo una hoja
					arbol.setHc(aux);
					aux = new TArbol();
					aCom.arbolCompleta(aux, 0, 0);//creo una hoja
					arbol.setHd(aux);					
					if (hojasHi>pasos*2)
						eliminaRamasInutiles(arbol.getHi(),pasos);
					
				}
				else{
					 int hojasHc = arbol.getHc().getNumHojas();
					 if (hojasHi + hojasHc>pasos){
						creaArbolCompleta aCom = new creaArbolCompleta();
						TArbol aux = new TArbol();
						aCom.arbolCompleta(aux, 0, 0);//creo una hoja
						arbol.setHd(aux);
					 }
					
				}
				
				
			}
			else{
				if (arbol.getValor().equals("PROGN2")){
					int hojasHi = arbol.getHi().getNumHojas();
					if (hojasHi>pasos){
						creaArbolCompleta aCom = new creaArbolCompleta();
						TArbol aux = new TArbol();
						aCom.arbolCompleta(aux, 0, 0);//creo una hoja
						arbol.setHd(aux);
						if (hojasHi>pasos*2)
							eliminaRamasInutiles(arbol.getHi(),pasos);
					}
				}
			}
			
		}
	}
}
		
		
		
		
		
		
		/*creaArbolCompleta aCom = new creaArbolCompleta();
		if (arbol.isEsHoja())
			return;//no se elimina nada
		
		if (pasos==0){
			aCom = new creaArbolCompleta();
			TArbol aux = new TArbol();
			aCom.arbolCompleta(aux, 0, 0);//creo una hoja
			arbol = aux;
		}
		else{
			int hojasHi= arbol.getHi().getNumHojas();
			if (hojasHi>pasos){			
				//En este caso, el arbol izquierdo es suficiente, por lo que debo deshacerme del central(si existe) y el derecho
				aCom = new creaArbolCompleta();
				TArbol aux;
				
				if (arbol.getValor().equals("PROGN3")){
					aux = new TArbol();
					aCom.arbolCompleta(aux, 0, 0);//creo una hoja
					arbol.setHc(aux);
					aux = new TArbol();
					aCom.arbolCompleta(aux, 0, 0);//creo una hoja
					arbol.setHd(aux);
				}
				else{
					if (arbol.getValor().equals("PROGN2")){
					aux = new TArbol();
					aCom.arbolCompleta(aux, 0, 0);//creo una hoja
					arbol.setHd(aux);
					}
				}
				eliminaRamasInutiles(arbol.getHi(),pasos);
				
			}
			else{
				if (arbol.getValor().equals("PROGN3")){
					if(arbol.getHi().getNumHojas() + arbol.getHc().getNumHojas()>pasos){
						TArbol aux = new TArbol();
						aCom = new creaArbolCompleta();
						aCom.arbolCompleta(aux, 0, 0);//creo una hoja
						arbol.setHd(aux);
						eliminaRamasInutiles(arbol.getHc(),pasos-arbol.getHi().getNumHojas());
						
						
					}
					else{
						if(arbol.getHi().getNumHojas() + arbol.getHc().getNumHojas()+ arbol.getHd().getNumHojas()>pasos){
							eliminaRamasInutiles(arbol.getHd(),pasos - arbol.getHi().getNumHojas() - arbol.getHc().getNumHojas());
						}
					}
					
				}
				else{
					if (arbol.getValor().equals("PROGN2"))
					if ((arbol.getHd().getNumHojas()+arbol.getHi().getNumHojas()>pasos))
					  eliminaRamasInutiles(arbol.getHd(),pasos-arbol.getHi().getNumHojas());
					
				}
			}
		}
		
	}*/




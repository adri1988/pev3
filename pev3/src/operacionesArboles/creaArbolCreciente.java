package operacionesArboles;

import utils.Funciones;
import utils.TArbol;
import utils.Terminales;
import utils.utils;

public class creaArbolCreciente {
	
	public void arbolCreciente(TArbol arbol,int hMax,int hMaxima){ 
		/*
		 * función inicializacionCreciente(profundidad) {
			si profundidad < maximaProdundidad árbol entonces
			nodo ← aleatorio(conjFunciones  conjTerminales)
			para i = 1 hasta número de hijos del nodo hacer
			Hijoi = inicializacionCreciente(profundidad+1 )
			eoc
			nodo ← aleatorio(conjTerminales)
			devolver nodo
	}*/
		arbol.setProfundidad(hMax);
		if (hMax < hMaxima){
			int tipo = utils.generaAleatorioEntre0y1();
			if (tipo==1){// si es tipo = 1 significa que vamos a generar funciones
				String operador = utils.operador_aleatorio();
				arbol.setValor(operador);	
				if (operador.equals(Funciones.PROGN3.toString())){
					int numNodos=0;
					int numHojas=0;
					arbol.setHi(new TArbol());
					arbol.setHc(new TArbol());
					arbol.setHd(new TArbol());	
					
					arbolCreciente(arbol.getHi(),hMax+1,hMaxima);
					setValoresArbol(arbol,0,true,hMax);	
					
					
					arbolCreciente(arbol.getHc(),hMax+1,hMaxima);
					setValoresArbol(arbol,1,true,hMax);	
					
					
					
					arbolCreciente(arbol.getHd(),hMax+1,hMaxima);
					setValoresArbol(arbol,2,true,hMax);	
					
					numNodos = arbol.getHi().getNumNodos() +  arbol.getHd().getNumNodos() +  arbol.getHc().getNumNodos() +1;
					arbol.setNumNodos(numNodos);
					
					numHojas = arbol.getHi().getNumHojas() +  arbol.getHd().getNumHojas()+  arbol.getHc().getNumHojas();	
					arbol.setNumHojas(numHojas);
					
				}
				else{
					int numNodos=0;
					int numHojas=0;
					arbol.setHi(new TArbol());
					arbol.setHd(new TArbol());
					
					
					arbolCreciente(arbol.getHi(),hMax+1,hMaxima);
					setValoresArbol(arbol,0,true,hMax);	
					
					
					arbolCreciente(arbol.getHd(),hMax+1,hMaxima);
					setValoresArbol(arbol,2,true,hMax);	
					numNodos = arbol.getHi().getNumNodos() +  arbol.getHd().getNumNodos() +1;
					arbol.setNumNodos(numNodos);
					arbol.setHc(null);
					
					numHojas = arbol.getHi().getNumHojas() +  arbol.getHd().getNumHojas();	
					arbol.setNumHojas(numHojas);
					
				}
			}	
			else{// si es tipo==2 significa que vamos a meter una hoja
				double num =  Math.random();
				if (num<0.8){				
					arbol.setValor("AVANZA");
					arbol.setEsHoja(true);
					arbol.setNumNodos(1);		
					arbol.setNumHojas(1);
				}
				else{
					String operando = utils.operando_aleatorio();
					arbol.setValor(operando);
					arbol.setEsHoja(true);
					arbol.setNumNodos(1);		
					arbol.setNumHojas(1);
				}
				
			}
		}
		
		else{
			double num =  Math.random();
			if (num<0.8){
				arbol.setValor("AVANZA");
				arbol.setEsHoja(true);
				arbol.setNumNodos(1);		
				arbol.setNumHojas(1);
			}
			else{
				String operando = utils.operando_aleatorio();
				arbol.setValor(operando);
				arbol.setEsHoja(true);
				arbol.setNumNodos(1);	
				arbol.setNumHojas(1);
			}
		}
		
	}
	
	private void setValoresArbol(TArbol arbol2, int hijoX, boolean b, int hMAx) {
		/*
		 * hijoX == 0 esHi
		 * hijoX == 1 esHc
		 * hijoX == 2 esHd
		 * */
		arbol2.setProfundidad(hMAx);
		
		switch (hijoX){
		case 0: arbol2.getHi().setEsHi(true);
				arbol2.getHi().setPadre(arbol2);
		break;
		case 1: arbol2.getHc().setEsHc(true);
				arbol2.getHc().setPadre(arbol2);
		break;
		case 2: arbol2.getHd().setEsHd(true);
				arbol2.getHd().setPadre(arbol2);
		break;
		default: break;
			
		}
		
	}
	
	

}

package selecciones;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import algoritmoEvolutivo.cromosoma;


public class seleccionPorTruncamiento {

	public void ejecuta(ArrayList<cromosoma> pob) {
	
		int[] sel_super = new int[pob.size()];//seleccionados para sobrevivir
		int[] indices = new int[pob.size()];//indices 
		ArrayList<cromosoma> nueva_pob= new ArrayList<cromosoma>(pob.size());  
		HashMap<Integer, Float> list =new HashMap<>();
		for (int i=0;i<pob.size();i++){
			list.put(i, pob.get(i).getAptitud());			
		}
		list=(HashMap<Integer, Float>) sortByValue(list);
		 double trunc = 0.2;//usamos el 20% para el umbral
		 int cantidadAptos = (int) (pob.size()*trunc);
		 int repeticiones = pob.size()/cantidadAptos;
		 
		 
		 
		 Iterator it = list.entrySet().iterator();
		 int p=0;
		 while (it.hasNext()){//Al salir de este bucle, en el array indices, voy a tener los indices de los individuos ordenados de menor a mayor
			 Map.Entry pair = (Map.Entry)it.next();
			 int indice  = (int) pair.getKey();
			 indices[p]=indice;
			 p++;
		 }
		 int desplazamiento=0;
		 for (int i=0;i<cantidadAptos;i++){	
			 int indice; 
				 indice = indices[pob.size()-i-1]; 
			 
			 for (int j=0;j<repeticiones;j++){	
				 sel_super[desplazamiento]=indice;
				 desplazamiento++;
			 }
		 }
		 
		 
		// if (desplazamiento!=tam_pob)
			 //rellenas restantes
			 
		 for (int i=0;i<pob.size();i++){//genero la población intermedia			
			nueva_pob.add(new cromosoma(pob.get(sel_super[i])));
			
		 }
		 pob = nueva_pob;//actualizo la población
		
	}
	
	private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map )
	{
	    List<Map.Entry<K, V>> list =new LinkedList<>( map.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<K, V>>()
	    {
	        @Override
	        public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
	        {
	            return (o1.getValue()).compareTo( o2.getValue() );
	        }
	    } );

	    Map<K, V> result = new LinkedHashMap<>();
	    for (Map.Entry<K, V> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}

}

package selecciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;

public class seleccionPorRanking {
	
	public void seleccionRanking(ArrayList<cromosoma> pob){
		/* En la seleccion por ranking se ordenan los individuos por su fitness y se les asigna
		   un valor de 1 a tam_pob.
		   Ordenamos de peor a mejor, asignamos valores de 1 a tam_pob, obtenemos los
		   acumulados de cada individuo, rellenamos la nueva poblacion.
		*/
		boolean mejorPoblacion=false;
		int contador=0;
		int media =calculaMediaDePoblacion(pob);		
		ArrayList<cromosoma> nueva_pob = null;
		
		
	//	while (!mejorPoblacion && contador<20){
		
		
		
		double prob; // probabilidad de seleccion
		nueva_pob= new ArrayList<cromosoma>(pob.size()); 
		ArrayList<cromosoma> ordenados= pob;
		
		for(int i=0;i<(pob.size()-1);i++){//ordenamos los cromosomas por aptitud.(mayor distancia a menor)
            for(int j=i+1;j<pob.size();j++){
                if(ordenados.get(i).getAptitud()<ordenados.get(j).getAptitud()){
                    //Intercambiamos valores
                    cromosoma aux=ordenados.get(i);
                    ordenados.set(i,ordenados.get(j));
                    ordenados.set(j, aux);
                }
            }
        }
		
		
		
		
		int[] acumulados = new int[pob.size()];
		acumulados[0]=1;
		for(int i=1; i<pob.size(); i++){//asignamos un valor acumulado dependiendo del ranking
			acumulados[i] = acumulados[i-1] + i+1;
		}
		for(int i=0; i<pob.size(); i++){
			prob = randomWithRange(0,acumulados[acumulados.length-1]);
			if(prob>=0 && prob<=acumulados[0]){
				nueva_pob.add(new cromosoma(ordenados.get(0)));
			}else{
				for(int j=1; j<pob.size(); j++){
					if(prob<=acumulados[j] && prob>acumulados[j-1]){//encontramos el individuo seleccionado y lo insertamos en la nueva pob		
						nueva_pob.add(new cromosoma(ordenados.get(j)));
						j=pob.size();
					}
				}
			}
		}		
		//contador++;	
		//int nuevaMedia = calculaMediaDePoblacion(nueva_pob);
		//if (nuevaMedia>media)
		//	mejorPoblacion=true;
		//}
		pob = nueva_pob;
		
	}
	
	int randomWithRange(int min, int max){
		   int range = (max - min) + 1;     
		   return (int)(Math.random() * range) + min;
		}
	
	private int calculaMediaDePoblacion(ArrayList<cromosoma> pob) {
		
		
		int media =0;
		for (int i=0;i<pob.size();i++){
			media+=pob.get(i).getAptitud();
		}
		media = media/pob.size();
		return media;
	}

}

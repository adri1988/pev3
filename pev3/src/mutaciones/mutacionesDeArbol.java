package mutaciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;
import operacionesArboles.creaArbolCompleta;
import operacionesArboles.creaArbolCreciente;
import utils.TArbol;
import utils.utils;

public class mutacionesDeArbol {

	public void ejecuta(ArrayList<cromosoma> pob, double prob_mut,int profundidad,int pasos) {
		
		int contadorDeEmpeorados=0;
		for (int i=0;i<pob.size();i++){
			double numAle = Math.random();
			if (numAle<prob_mut){
				TArbol aux = utils.copiaArbol(pob.get(i).getArbol());
				int aptitud = (int) pob.get(i).getAptitud();
			    setSubArbolDeFormaAleatoria(pob.get(i).getArbol(),profundidad);
				int nuevaAptitud = (int) pob.get(i).evalua(pasos);
				
						
				boolean mejor=aptitud< nuevaAptitud;			
				int contador=0;
			
				while (!mejor && contador<5){
					
					setSubArbolDeFormaAleatoria(pob.get(i).getArbol(),profundidad);
					nuevaAptitud = (int) pob.get(i).evalua(pasos);
					if (nuevaAptitud>aptitud)
						mejor=true;
					contador++;
					
				}
				if (!mejor)
					pob.get(i).setArbol(aux);
				
				
				
			}
		}
		/*if (contadorDeEmpeorados!=0){
			System.out.println("total de individuos peores despues de la mutacion :" + contadorDeEmpeorados);
		}*/
		
		
	}
	//implementar

	private void setSubArbolDeFormaAleatoria(TArbol arbol,int profundidad) {
	
		//System.out.println(arbol);
		creaArbolCreciente aCre = new creaArbolCreciente();
		TArbol subArbol = new TArbol();
		int numNodos = arbol.getNumNodos();
				
		aCre.arbolCreciente(subArbol, 1, profundidad-1);
		//System.out.println("SubArbol " + subArbol);
		TArbol aux;
		int nodo=0;
			
		if (numNodos==1)
			nodo = 0;
		else
			nodo = utils.randomWithRange(1, numNodos-1);
		
		 aux = arbol.BuscarNodo(nodo); 
			//yo tengo el nodo del arbol, ahora vamos a reemplazarlo,
		//básicamente vamos a sustituir todos los valores, con los valores del subArbol generado
		
	
		aux.setEsHc(subArbol.isEsHc());
		aux.setEsHd(subArbol.isEsHd());
		aux.setEsHi(subArbol.isEsHi());
		aux.setEsHoja(subArbol.isEsHoja());
		aux.setHc(subArbol.getHc());
		aux.setHd(subArbol.getHd());
		aux.setHi(subArbol.getHi());
		aux.setNumHojas(subArbol.getNumHojas());
		aux.setNumNodos(subArbol.getNumNodos());		
		aux.setProfundidad(subArbol.getProfundidad());
		aux.setValor(subArbol.getValor());
		utils.actualizaArbol(arbol,0);
	
		
		
		
		
		
		
	}
	
	
private void setTerminalAleatorio(TArbol arbol) {
		
		
		String operando = utils.operando_aleatorio();
		int numNodos = arbol.getNumNodos();
		boolean encontrado =false;
		TArbol aux=null;
		int nodo = 0;
		while (!encontrado){
			nodo = utils.randomWithRange(1, numNodos-1);
			aux = arbol.BuscarNodo(nodo);
			if (aux.isEsHoja())
				encontrado=true;			
		}
		
		aux.setValor(operando);
		
		
	}
}

package edades;

import algoritmoEvolutivo.cromosoma;
import utils.utils;

public class EdadProporcional {

	public void ejecuta(cromosoma c,int aptMed, int gen_actual) {
		
		
		int edad=0;
		
		edad = (int) Math.min(utils.maxTV, utils.minTV + utils.diftTV* (c.getAptitud()/aptMed));
		
		c.setEdad(edad+gen_actual);
		c.setEdadYaCalculada(true);
		
	}

}

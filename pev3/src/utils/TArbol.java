package utils;

public class TArbol {
	
	private TArbol hi;
	private TArbol hc; // hijo central 	
	private TArbol hd;
	private TArbol padre;
	private int profundidad;
	private String valor;
	private boolean esRaiz= false;
	private boolean esHoja= false;
	private int numNodos=0;
	private boolean esHi = false;
	private boolean esHd= false;
	private boolean esHc= false;
	private int posSimbolo;	
	private int numHojas=0;
	
	public TArbol BuscarNodo(int nodo_cruce) {
		int nodoActual =0;
		
		return recorridoPreordenParaNodo(this,nodo_cruce,nodoActual);
		
		
		
	}
	private TArbol recorridoPreordenParaNodo(TArbol tArbol, int nodo_cruce, int nodoActual) {
		if (nodo_cruce==nodoActual)
			return tArbol;
		else{
			if (tArbol.getValor().equals( Funciones.PROGN3.toString())){
				if (tArbol.getHi().getNumNodos()+nodoActual>=nodo_cruce){
					//System.out.println(tArbol.getHi());
					//System.out.println(tArbol.getHi().getNumNodos() + nodoActual +  " >= " + nodo_cruce);
					return recorridoPreordenParaNodo(tArbol.getHi(),nodo_cruce,nodoActual+1);
				}
					
				if (tArbol.getHc().getNumNodos() + tArbol.getHi().getNumNodos()+nodoActual >=nodo_cruce){
					//System.out.println(tArbol.getHc());
					//System.out.println(tArbol.getHi().getNumNodos() +tArbol.getHc().getNumNodos()+ nodoActual +  " >= " + nodo_cruce);
					return recorridoPreordenParaNodo(tArbol.getHc(),nodo_cruce,tArbol.getHi().getNumNodos()+nodoActual+1);
				}
					
				
				return recorridoPreordenParaNodo(tArbol.getHd(),nodo_cruce,tArbol.getHi().getNumNodos()+tArbol.getHc().getNumNodos()+nodoActual+1);
			}
			else{
				if (tArbol.getHi().getNumNodos()+nodoActual>=nodo_cruce){
					//System.out.println(tArbol.getHi());
					//System.out.println(tArbol.getHi().getNumNodos() + nodoActual +  " >= " + nodo_cruce);
					return recorridoPreordenParaNodo(tArbol.getHi(),nodo_cruce,nodoActual+1);
				}
					
				return recorridoPreordenParaNodo(tArbol.getHd(),nodo_cruce,tArbol.getHi().getNumNodos()+nodoActual+1);
			}
		}	
		
		
	}
	
	
	public void actualizaArbol() {
	
		
		actualizaEnPreOrden(this,0);	
		
		
		
		
}
	private void actualizaEnPreOrden(TArbol tArbol, int i) { //actualiza tanto el número de nodos como de hojas
		
		if (tArbol.isEsHoja()){//si estamos en una hoja, el número de nodos es 1
			 tArbol.setNumNodos(1);
			 tArbol.setNumHojas(1);
		}
		else{			
			actualizaEnPreOrden(tArbol.getHi(),i+1);
			actualizaEnPreOrden(tArbol.getHc(),i+1);
			actualizaEnPreOrden(tArbol.getHd(),i+1);
			tArbol.setNumNodos(tArbol.getHi().getNumNodos()+tArbol.getHc().getNumNodos()+tArbol.getHd().getNumNodos());		
			tArbol.setNumHojas(tArbol.getHi().getNumeroHojas(tArbol.getHi())+tArbol.getHc().getNumeroHojas(tArbol.getHc())+tArbol.getHd().getNumeroHojas(tArbol.getHd()));
		}
		
	}
	
	public int getNumeroHojas(TArbol arbol){
		int acum=0;
		if (arbol.isEsHoja())
			return 1;
		else{
			
			if (arbol.getValor().equals( Funciones.PROGN3.toString())){
				acum = getNumeroHojas(arbol.getHi());
				acum+=getNumeroHojas(arbol.getHc());
				acum+=getNumeroHojas(arbol.getHd());
			}
			else{
				acum = getNumeroHojas(arbol.getHi());
				acum+=getNumeroHojas(arbol.getHd());
			}			
		}
		
		return acum;
	}
	public TArbol getHi() {
		return hi;
	}
	public void setHi(TArbol hi) {
		this.hi = hi;
	}	
	public TArbol getHc() {
		return hc;
	}
	public void setHc(TArbol hc) {
		this.hc = hc;
	}
	public TArbol getHd() {
		return hd;
	}
	public void setHd(TArbol hd) {
		this.hd = hd;
	}
	public TArbol getPadre() {
		return padre;
	}
	public void setPadre(TArbol padre) {
		this.padre = padre;
	}
	public int getProfundidad() {
		return profundidad;
	}
	public void setProfundidad(int profundidad) {
		this.profundidad = profundidad;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public boolean isEsRaiz() {
		return esRaiz;
	}
	public void setEsRaiz(boolean esRaiz) {
		this.esRaiz = esRaiz;
	}
	public boolean isEsHoja() {
		return esHoja;
	}
	public void setEsHoja(boolean esHoja) {
		this.esHoja = esHoja;
	}
	public int getNumNodos() {
		return numNodos;
	}
	public void setNumNodos(int numNodos) {
		this.numNodos = numNodos;
	}
	public boolean isEsHi() {
		return esHi;
	}
	public void setEsHi(boolean esHi) {
		this.esHi = esHi;
	}
	public int getPosSimbolo() {
		return posSimbolo;
	}
	public void setPosSimbolo(int posSimbolo) {
		this.posSimbolo = posSimbolo;
	}
	public boolean isEsHd() {
		return esHd;
	}
	public void setEsHd(boolean esHd) {
		this.esHd = esHd;
	}
	
	public boolean isEsHc() {
		return esHc;
	}
	public void setEsHc(boolean esHc) {
		this.esHc = esHc;
	}
	public int getNumHojas() {
		return numHojas;
	}
	public void setNumHojas(int numHojas) {
		this.numHojas = numHojas;
	}
	@Override
	public String toString() {
		if (this.esHoja)
			return this.valor;
		else{
			if (this.hc!=null)
				return this.valor + " (" +this.hi.toString() + " " + this.hc.toString() + " " + this.hd.toString() + " ) ";
			else
				return this.valor + " (" + this.hi.toString() + " " + this.hd.toString() + " ) ";
		}
			
	}
	public void setValoresArbol(TArbol subarbol2) {
		this.setEsHc(subarbol2.isEsHc());
		this.setEsHd(subarbol2.isEsHd());
		this.setEsHi(subarbol2.isEsHi());
		this.setEsHoja(subarbol2.isEsHoja());
		this.setHc(subarbol2.getHc());
		this.setHd(subarbol2.getHd());
		this.setHi(subarbol2.getHi());
		this.setNumHojas(subarbol2.getNumHojas());
		this.setNumNodos(subarbol2.getNumNodos());		
		this.setProfundidad(subarbol2.getProfundidad());
		this.setValor(subarbol2.getValor());
		utils.actualizaArbol(this,0);
		
	}
	
	
	

	
	

}

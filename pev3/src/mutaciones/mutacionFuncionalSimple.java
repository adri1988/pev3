package mutaciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;
import utils.TArbol;
import utils.utils;

public class mutacionFuncionalSimple {

	public void ejecuta(ArrayList<cromosoma> pob, double prob_mut,int pasos) {
		
		for (int i=0;i<pob.size();i++){
			double numAle = Math.random();
			if (numAle<prob_mut){
				
				setFuncionAleatorio(pob.get(i).getArbol());	
				
				pob.get(i).evalua(pasos);
			}
		}
		
	}
	//implementar

	private void setFuncionAleatorio(TArbol arbol) {
		

		if (!arbol.isEsHoja()){//si no es hoja, hacemos cosas, sino, nos vamos
				
			int numNodos = arbol.getNumNodos();			
			boolean[] nodoBuscado = new boolean[numNodos];	
			for (int i=0;i<numNodos;i++)
				nodoBuscado[i]=false;
			int contadorDeNodosDisitntos=0;
			
			boolean encontrado =false;
			TArbol aux=null;
			int nodo = 0;
			while (!encontrado && contadorDeNodosDisitntos<numNodos){
				
				nodo = utils.randomWithRange(0, numNodos-1);
				while (nodoBuscado[nodo])
					nodo = utils.randomWithRange(0, numNodos-1);
				nodoBuscado[nodo]=true;
				contadorDeNodosDisitntos++;
				
				
				aux = arbol.BuscarNodo(nodo);
				if (!aux.isEsHoja() && aux.getHc()==null){
					if (aux.getValor().equals("PROGN2"))
						aux.setValor("SIC");
					else
						aux.setValor("PROGN2");
					encontrado=true;
					
				}
								
			}
			
			
		}
		
	}
}

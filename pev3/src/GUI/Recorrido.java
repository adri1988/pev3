package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

import algoritmoEvolutivo.cromosoma;
import utils.TArbol;
import utils.utils;

import java.util.ArrayList;
import java.util.Random;



public class Recorrido extends JPanel {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width, height;
    private int ruut; // square 
    private int w = width / 2, h = height / 2; // middle of the app 
    private BufferedImage buffer;
    private BufferedImage antD;
    private BufferedImage antI;
    private BufferedImage antA;
    private BufferedImage antAb;
    private BufferedImage food;
    private BufferedImage sinPasar;
    private BufferedImage hePasadoNoComida;
    private BufferedImage hePasadoComida;
    private BufferedImage antConDir;
   
    Random rand = new Random();
    TArbol arbol;
    int posicionX;
    int posicionY;
    int direccion =1;
	private int limite = 32;
	int indiceArrayGlobal=0;
	int bocados=0;
	int pasos=0;
	String comidaH="#";
	String[][] mapaSantaFe;
	ArrayList<String> movimientos;
	
	
	 String[][] santaFe = {
				
				{"@","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","#","#","0","0","0","0"},
				{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","#","0","0"},
				{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","#","0","0"},
				{"0","0","0","#","#","#","#","0","#","#","#","#","#","0","0","0","0","0","0","0","0","#","#","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","#","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","#","#","#","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","#","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0"},
				{"0","0","0","#","#","0","0","#","#","#","#","#","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","#","0","0","0","0","0","0","#","#","#","#","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","#","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","#","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
				{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"}
				};

	public Recorrido(cromosoma arbol) throws IOException {
		 
		  this(800, 800, 25,arbol.getArbol(),(int) arbol.getAptitud());    
	}

	public Recorrido(int width, int height, int ruut,TArbol arbol,int aptitud) throws IOException {
		    this.width = width;
	        this.height = height;
	        this.ruut = ruut;
	        this.arbol=arbol;	        
	        this.posicionX=0;
			this.posicionY=0;
			this.direccion=1;
	        this.buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	        this.indiceArrayGlobal=0;
	        System.out.println(aptitud);
	        antD = ImageIO.read(new File("antD.png"));
	        antI = ImageIO.read(new File("antI.png"));
	        antA = ImageIO.read(new File("antA.png"));
	        antAb = ImageIO.read(new File("antAb.png"));
	        food = ImageIO.read(new File("food.png"));
	        sinPasar = ImageIO.read(new File("sinPasar.jpg"));
	        hePasadoNoComida = ImageIO.read(new File("hePasadoNoComida.jpg"));
	        hePasadoComida = ImageIO.read(new File("hePasadoComida.png"));
	        antConDir = antD;
	        setBackground(Color.BLACK); 
	        setPreferredSize(new Dimension(width, height));
	        setDoubleBuffered(false);
	        pintaTableroInicial(buffer.getGraphics());
	        movimientos = new ArrayList<String>();
	        initMapaSantaFe();
	       
	        setMovimientos(arbol);
	        
	        System.out.println("El mejor en el recorrido");
			System.out.println(movimientos);
	       // System.out.println(movimientos);
	        
	       /* for (int i=0;i<32;i++){
	        	  for (int j=0;j<32;j++){
	        		  System.out.print(santaFe[i][j]+",");
	        	  }
	        	  System.out.println();
	        	
	        }*/
	        
	        initMapaSantaFe();
	        
	        //System.out.println(arbol);
	       // System.out.println(movimientos);
	        posicionX=0;
	        posicionY=0;
	        direccion =1;
	        
	        new Timer(100, new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	
	                try {
						moveNext(buffer.getGraphics());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	            }

				
	        }).start();
	}
	
	 private void setMovimientos(TArbol arbol2) {
		 if (pasos<400 && bocados<88){	
			
				
				/*else{
					santaFe[posicionX][posicionY] = "-1";
				}*/
				
				switch (arbol2.getValor()){
					case "PROGN3":
						setMovimientos(arbol2.getHi());
						setMovimientos(arbol2.getHc());
						setMovimientos(arbol2.getHd());					
						break;
					case "PROGN2":
						setMovimientos(arbol2.getHi());
						setMovimientos(arbol2.getHd());
						break;
					case "SIC":
						if (hayComida())
							setMovimientos(arbol2.getHi());				
						else
							setMovimientos(arbol2.getHd());	
						break;
					case "AVANZA":
						avanza();
						if (santaFe[posicionY][posicionX].equals(comidaH)){
							santaFe[posicionY][posicionX] = "1";//el 1 significa que he comido en esa posición
							bocados++;
						}
						pasos++;
						break;
					case "GIRA_DERECHA":
						derecha();
						pasos++;
						break;
					case "GIRA_IZQUIERDA":
						izquierda();
						pasos++;
						break;
					default:break;				
						
				}
				
			}
			//if (pasos >= 400)
			//	System.out.println("Pasos: "+pasos + "  y bocados: "+ bocados );
			if (bocados>=88)
				System.out.println("game over");
		
	}

	

	public void paintComponent(Graphics g){
	        super.paintComponent(g);
	        g.drawImage(buffer, 0, 0, this);
	    }
	
	private void moveNext(Graphics g) throws IOException {
		
	        if (!(indiceArrayGlobal+1>movimientos.size())){
	        String movimiento = movimientos.get(this.indiceArrayGlobal);
	        switch (movimiento){
	        case "AVANZA":
	        	avanza2();
	        	break;
	        case "GIRA_DERECHA":
	        	derecha2();
	        	break;
	        case "GIRA_IZQUIERDA": 
	        	izquierda2();
	        	break;
	        default: break;
	        }
	        
	        if (santaFe[this.posicionY][this.posicionX].equals(this.comidaH))
	        	santaFe[this.posicionY][this.posicionX]="1";
	        else{
	        	if (santaFe[this.posicionY][this.posicionX].equals("1"))
	        		santaFe[this.posicionY][this.posicionX]="1";
	        	else
	        		santaFe[this.posicionY][this.posicionX]="-1";
	        }
	        	
	        
	        pintaTableroEntero(g);	
	        
	        this.indiceArrayGlobal++;
	        }

	     
	     /*   BufferedImage img = ImageIO.read(new File("bien.jpg"));
	        g.drawImage(img, w, h, null);*/
	        //g.fillRect(w, h, ruut, ruut);
	        repaint();
		
	}
	
	 
	  
	  
	
	  
	  private void pintaTableroEntero(Graphics g) {
			
		  /*   antD = ImageIO.read(new File("antD.png"));
	        antI = ImageIO.read(new File("antI.png"));
	        antA = ImageIO.read(new File("antA.png"));
	        antAb = ImageIO.read(new File("antAb.png"));
	        food = ImageIO.read(new File("food.png"));
	        sinPasar = ImageIO.read(new File("sinPasar.jpg"));
	        hePasadoNoComida = ImageIO.read(new File("hePasadoNoComida.jpg"));
	        hePasadoComida = ImageIO.read(new File("hePasadoComida.png")); */
	        BufferedImage img = null;
	    for (int i=0;i<32;i++){
	    	for (int j=0;j<32;j++){
	    	
			switch (santaFe[i][j]){
		   		  case "0":img =sinPasar;
		   			  break;
		   		  case "#":
		   			  img =food;
		   			  break;
		   		  case "@":
		   			  img = antD;
		   		  case "1": img = hePasadoComida;
		   			  break;
		   		  case "-1":img = hePasadoNoComida;
		   			  break;
	   		  }
			g.drawImage(img, j*25, i*25, null);
	    	}
	    }
	    g.drawImage(antConDir, this.posicionX*25, this.posicionY*25, null);
	    
	    
	    
	    
		
		 
	        //g.fillRect(w, h, ruut, ruut);
		
	}

	private void izquierda() {
			switch (direccion){
			case 0:
				direccion=3;
				movimientos.add("GIRA_IZQUIERDA");
				break;
			case 1:
				direccion=0;
				movimientos.add("GIRA_IZQUIERDA");
				break;
			case 2:
				direccion=1;
				movimientos.add("GIRA_IZQUIERDA");
				break;
			case 3:
				direccion=2;
				movimientos.add("GIRA_IZQUIERDA");
				break;
			}
			
		}
	  
	  private void izquierda2() {
			switch (direccion){
			case 0:
				direccion=3;
				antConDir = antI;
				
				break;
			case 1:
				direccion=0;
				antConDir = antA;
				
				break;
			case 2:
				direccion=1;
				antConDir = antD;
			
				break;
			case 3:
				direccion=2;
				antConDir = antAb;
				
				break;
			}
			
		}

		private void derecha() {
			switch (direccion){
			case 0:
				direccion=1;
				movimientos.add("GIRA_DERECHA");
				break;
			case 1:
				direccion=2;
				movimientos.add("GIRA_DERECHA");
				break;
			case 2:
				direccion=3;
				movimientos.add("GIRA_DERECHA");
				break;
			case 3:
				direccion=0;
				movimientos.add("GIRA_DERECHA");
				break;
			}
			
		}
		
		private void derecha2() {
			switch (direccion){
			case 0:
				direccion=1;
				antConDir = antD;
			
				break;
			case 1:
				direccion=2;
				antConDir = antAb;
				
				break;
			case 2:
				direccion=3;
				antConDir = antI;
				
				break;
			case 3:
				direccion=0;
				antConDir = antA;
				
				break;
			}
			
		}
		
		/*
		0-norte
		1-este
		2-sur
		3 oeste
	*/

		private void avanza() {
			
			switch (direccion){
			case 0:
			
				//	System.out.print("AVANZA,");
					movimientos.add("AVANZA");
					if (posicionY==0)
						posicionY=31;
					else
						posicionY--;
			
					
			
				break;
			case 1:
				    if (posicionX==31)
				    	posicionX=0;
				    else
				    	posicionX++;
				//	System.out.print("AVANZA,");
					movimientos.add("AVANZA");
				
					
				break;
			case 2:
		            if (posicionY==31)
		            	posicionY=0;
		            else	            	
		            	posicionY++;
					//System.out.print("AVANZA,");
					movimientos.add("AVANZA");
				
				
				break;
			case 3:
				     if (posicionX==0)
				    	 posicionX=31;
				     else
				    	 posicionX--;
				//	System.out.print("AVANZA,");
					movimientos.add("AVANZA");
				
				
				break;
			}
			
			
			//mapaSantaFe[posicionX][posicionY]="1";
			
			
		}
		
private void avanza2() {//avanzar igual pero sin modificar el array movimientos
			
	switch (direccion){
	case 0:
	
		//	System.out.print("AVANZA,");
			
			if (posicionY==0)
				posicionY=31;
			else
				posicionY--;
	
			
	
		break;
	case 1:
		    if (posicionX==31)
		    	posicionX=0;
		    else
		    	posicionX++;
		//	System.out.print("AVANZA,");
		
		
			
		break;
	case 2:
            if (posicionY==31)
            	posicionY=0;
            else	            	
            	posicionY++;
			//System.out.print("AVANZA,");
		
		
		
		break;
	case 3:
		     if (posicionX==0)
		    	 posicionX=31;
		     else
		    	 posicionX--;
		//	System.out.print("AVANZA,");
			
		
		
		break;
	}
			
			
			//mapaSantaFe[posicionX][posicionY]="1";
			
			
		}

		private boolean sePuedeAvanzar() {
			boolean resultado=true;
			switch (direccion){
			case 0:		
				if (posicionX==0)
					resultado=false;
				break;
			case 1:			
				if (posicionY==31)
					resultado=false;
				break;
			case 2:
				if (posicionX==31)
					resultado=false;
				break;
			case 3:
				if (posicionY==0)
					resultado=false;
				break;
			}
			
			
			return resultado;
		}

		private boolean hayComida() {
			
			try{
				
				boolean comida=false;
					switch (direccion){
					case 0:
						if (posicionY==0){				
							if (santaFe[31][posicionX].equals(comidaH))
								comida=true;
						}
						else{
							if (santaFe[posicionY-1][posicionX].equals(comidaH))
								comida=true;
						}
						
							
						break;
					case 1:
							if (posicionX==31){
								if (santaFe[posicionY][0].equals(comidaH))
									comida=true;
							}
							else{
								if (santaFe[posicionY][posicionX+1].equals(comidaH))
									comida=true;
							}
								
						break;
					case 2:
							if (posicionY ==31){
								if (santaFe[0][posicionX].equals(comidaH))
									comida=true;
							}
							else{
								if (santaFe[posicionY+1][posicionX].equals(comidaH))
									comida=true;
							}
							
						break;
					case 3:
							if (posicionX==0){
								if (santaFe[posicionY][31].equals(comidaH))
									comida=true;
							}
							else{
								
								if (santaFe[posicionY][posicionX-1].equals(comidaH))
									comida=true;
								
							}
						
							
						break;
						default: break;
					}
					
					return comida;
				}
				catch (Exception e){
					System.out.println("La X "+posicionX);
					System.out.println("La Y "+posicionY);
					System.out.println("La Y "+posicionY);
					System.out.println(e.getMessage());
				}
			return false;
			
		}
		
		 private void pintaTableroInicial(Graphics g) {
			 
			 String[][] santaFeOriginal = {
						
						{"@","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","#","#","0","0","0","0"},
						{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","#","0","0"},
						{"0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","#","0","0"},
						{"0","0","0","#","#","#","#","0","#","#","#","#","#","0","0","0","0","0","0","0","0","#","#","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","#","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","#","#","#","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","#","0","0","#","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0"},
						{"0","0","0","#","#","0","0","#","#","#","#","#","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","#","0","0","0","0","0","0","#","#","#","#","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","#","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","#","#","#","#","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"},
						{"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"}
						};
			     initMapaSantaFe();
				
		    	for (int i=0;i<32;i++){
		    		for (int j=0;j<32;j++){
		        		pintaPieza(i,j,santaFe, g);
		        	}
		    	}
		    	//System.out.println("tablero pintado");
		    	 repaint();
			}

		private void pintaPieza(int i, int j, String[][] santaFeOriginal,Graphics g) {
			
			  BufferedImage img = null;
			  switch (santaFeOriginal[i][j]){
			  case "0":img =sinPasar;
				  break;
			  case "#":
				  img =food;
				  break;
			  case "@":
				  img = antD;
				  break;
			  }
			
			 g.drawImage(img, j*25, i*25, null);
		        //g.fillRect(w, h, ruut, ruut);
		    
			
		}
		
		private void pintaTablero(int i, int j, String[][] santaFeOriginal,Graphics g) {
			
			/*    antD = ImageIO.read(new File("antD.png"));
		        antI = ImageIO.read(new File("antI.png"));
		        antA = ImageIO.read(new File("antA.png"));
		        antAb = ImageIO.read(new File("antAb.png"));
		        food = ImageIO.read(new File("food.png"));
		        sinPasar = ImageIO.read(new File("sinPasar.jpg"));
		        hePasadoNoComida = ImageIO.read(new File("hePasadoNoComida.jpg"));
		        hePasadoComida = ImageIO.read(new File("hePasadoComida.png"));  */
			
			BufferedImage img = null;
			  switch (santaFeOriginal[i][j]){
			  case "0":img =hePasadoNoComida;
				  break;
			  case "#":
				  img =hePasadoComida;
				  break;
			  case "@":
				  img = antD;
				  break;
			  }
			
			 g.drawImage(img, j*25, i*25, null);
		        //g.fillRect(w, h, ruut, ruut);
		    
			
		}
	
		  @SuppressWarnings("unused")
		private void ejecutaArbol(TArbol arbol) {
				
				if (pasos<3000 && bocados<88){	
					if (posicionX>=32 || posicionY>=32)
						System.out.println("fallo");
					if (mapaSantaFe[posicionX][posicionY].equals(comidaH)){
						mapaSantaFe[posicionX][posicionY] = "1";//el 1 significa que he comido en esa posición
						bocados++;
					}
					
					//ahora vamos a ver el nodo en el que estamos y realizamos acción
					
					switch (arbol.getValor()){
						case "PROGN3":
							ejecutaArbol(arbol.getHi());
							ejecutaArbol(arbol.getHc());
							ejecutaArbol(arbol.getHd());					
							break;
						case "PROGN2":
							ejecutaArbol(arbol.getHi());
							ejecutaArbol(arbol.getHd());
							break;
						case "SIC":
							if (hayComida())
								ejecutaArbol(arbol.getHi());				
							else
								ejecutaArbol(arbol.getHd());	
							break;
						case "AVANZA":
							movimientos.add("AVANZA");
							pasos++;
							break;
						case "GIRA_DERECHA":
							movimientos.add("GIRA_DERECHA");
						//	pasos++;
							break;
						case "GIRA_IZQUIERDA":
							movimientos.add("GIRA_IZQUIERDA");
						//	pasos++;
							break;
						default:break;				
							
					}
					
				}
				if (pasos >= 3000)
					System.out.println("Pasos: "+pasos + "  y bocados: "+ bocados );
				if (bocados>=88)
					System.out.println("game over");
				
			}
		  
		  public void wallTest(){
		        if (h > height){
		            h = 0;
		        }
		        if (h < 0){
		            h = height;
		        }
		        if (w > width){
		            w = 0;
		        }
		        if (w < 0){
		            w = width;
		        }
		    }   
		  private void initMapaSantaFe() {
				santaFe = new String[this.limite][this.limite];		
				for (int i=0;i<this.limite;i++){
					for (int j=0;j<this.limite;j++){
						if (utils.MAPASANTAFE[i][j].equals("1"))
								utils.MAPASANTAFE[i][j]="#";	
						santaFe[i][j]=utils.MAPASANTAFE[i][j];
					}
				}
		  }

}

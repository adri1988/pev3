package edades;

import algoritmoEvolutivo.cromosoma;
import utils.utils;

public class EdadLineal {

	public void ejecuta(cromosoma c,int aptMin,int aptMax, int gen_actual) {
		
		int edad =0;
		edad = (int) (utils.minTV + 2*utils.diftTV * ( (c.getAptitud() - aptMin) / (aptMax - aptMin) ));
		c.setEdad(edad+gen_actual);
		c.setEdadYaCalculada(true);
		
	}

}

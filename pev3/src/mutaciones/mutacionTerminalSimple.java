package mutaciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;
import utils.*;

public class mutacionTerminalSimple {

	
	public void ejecuta(ArrayList<cromosoma> pob, double prob_mut,int pasos) {
		/*
		 *  char []terminales = new char[4];
			terminales[0] = 'a';terminales[1] = 'b';
			terminales[2] = 'c';terminales[3] = '2';
			for (int i = 0; i < this.Poblacion; i++) {
			Cromosoma c = (Cromosoma) individuos.get(i);
			double numAle = Math.random();
			if(numAle<this.PrMut){
			int numAle2 = (int) (Math.random()*4);
			Simbolo s = getTerminalAleatorio(c);
			s.setTerminal(terminales[numAle2]);
}
}
		 * 
		 * */
		
		
		for (int i=0;i<pob.size();i++){
			double numAle = Math.random();
			if (numAle<prob_mut){
				//System.out.println(pob.get(i).getArbol());
				setTerminalAleatorio(pob.get(i).getArbol());	
				//System.out.println(pob.get(i).getArbol());
				pob.get(i).evalua(pasos);
			}
		}
		
	}

	private void setTerminalAleatorio(TArbol arbol) {
		
		
		String operando = utils.operando_aleatorio();
		int numNodos = arbol.getNumNodos();
		boolean encontrado =false;
		TArbol aux=null;
		int nodo = 0;
		while (!encontrado){
			if (numNodos!=1)
				nodo = utils.randomWithRange(1, numNodos-1);
			else
				nodo=0;
			
			aux = arbol.BuscarNodo(nodo);
			if (aux.isEsHoja())
				encontrado=true;			
		}
		
		aux.setValor(operando);
		
		
	}
	
	
	
	
}

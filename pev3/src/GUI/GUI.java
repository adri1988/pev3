package GUI;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.border.CompoundBorder;

import algoritmoEvolutivo.algoritmoEvolutivo;
import configPanel.ConfigPanel;
import configPanel.ConfigPanel.*;
import configPanel.*;
import configPanel.ConfigPanel.ConfigListener;
import utils.*;


public class GUI extends JFrame {
	
private static final long serialVersionUID = 5393378737313833016L;
boolean esValido=false;	
boolean renderiza = false;
JPanel graph;
JPanel recorrido;
public int numMaxIter=0;
	public GUI() {
		
		super("Hormiga atómica");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		JPanel panelCentral = new JPanel(new GridLayout(4, 4,4,2));
		Checkbox contractividad = new Checkbox("Contractividad");
		Checkbox escalado = new Checkbox("Escalado");
		
		add(panelCentral, BorderLayout.EAST);
		//setLayout(new GridLayout(3,2));
		
		// crea dos figuras
		final algoritmoEvolutivo ae = new algoritmoEvolutivo();	
		
		
		// crea un panel central y lo asocia con la primera figura
		final ConfigPanel<algoritmoEvolutivo> cp = creaPanelConfiguracion();
		
		// asocia el panel con la figura
		cp.setTarget(ae);
		
		
		// carga los valores de la figura en el panel
		cp.initialize();
		
		
		
		add(cp, BorderLayout.WEST);
		//cp.add(contractividad);
		//cp.add(escalado);
		JTabbedPane tabbedPane = new JTabbedPane();		
		
		JPanel grafica = new JPanel();		
		JPanel recorrido = new JPanel();
		
		
		tabbedPane.addTab("Gráfica", null, grafica,"Does nothing");
		tabbedPane.addTab("Recorrido", null, recorrido,"Does nothing");
		tabbedPane.setFont(new Font("Calibri", Font.PLAIN, 12));
		
	   
		
		// crea una etiqueta que dice si todo es valido
		final String textoTodoValido = "Todos los campos OK";
		final String textoHayErrores = "Hay errores en algunos campos";
		final JLabel valido = new JLabel(textoHayErrores);
		valido.setFont(new Font("Calibri", Font.PLAIN, 12));
		// este evento se lanza cada vez que la validez cambia
		
		cp.addConfigListener(new ConfigListener() {
			@Override
			public void configChanged(boolean isConfigValid) {
				valido.setText(isConfigValid ? textoTodoValido: textoHayErrores);
				esValido = isConfigValid;
			}
		});
	
		
		JButton boton,boton2;
		
		boton2 = new JButton("Pinta Recorrido");
		boton2.setMnemonic('P');
		boton2.setBorderPainted(false);
		boton2.setBackground(Color.YELLOW);
		boton2.setBorder(null);
		boton2.setFocusable(false);
		boton2.setMargin(new Insets(0, 0, 0, 0));
		boton2.setEnabled(false);
		boton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cp.setTarget(ae);
				cp.initialize();
				if (esValido){ 	
				
					try {
						
						ae.getElMejor().evalua(ae.getPasos());						
						tabbedPane.setComponentAt(1, new Recorrido(ae.getElMejor()));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//tabbedPane.setTabComponentAt(index, component);
				}
			}
		});	
		
		boton = new JButton("Ejecuta AG");
		boton.setMnemonic('A');
		boton.setBorderPainted(false);
		boton.setBackground(Color.MAGENTA);
		boton.setBorder(null);
		boton.setFocusable(false);
		boton.setMargin(new Insets(0, 0, 0, 0));
		boton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cp.setTarget(ae);
				cp.initialize();
				if (esValido){ 
					if (numMaxIter>100)
						ae.inicializa(numMaxIter);
					else
						ae.inicializa();
					graph = ae.ejecuta();	
					tabbedPane.setComponentAt(0, graph);
					boton2.setEnabled(true);
					/*try {
						tabbedPane.setComponentAt(1, new Recorrido(ae.getElMejor().getArbol()));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
					//tabbedPane.setTabComponentAt(index, component);
				}
			}
		});	
		
		
		
		
		
		
		add(boton,BorderLayout.PAGE_START);
		add(boton2,BorderLayout.AFTER_LAST_LINE);
		
		add(tabbedPane,BorderLayout.CENTER);
		
		add(valido, BorderLayout.SOUTH);
		
		// crea una etiqueta que indica la figura que se esta editando
		final JLabel panelEnEdicion = new JLabel("Algoritmo genético");
		panelEnEdicion.setFont(new Font("Calibri", Font.ITALIC, 20));
		add(panelEnEdicion, BorderLayout.NORTH);
		
		
		
		
	}
	
	
	public ConfigPanel<algoritmoEvolutivo> creaPanelConfiguracion() {
		elitismo[] elit = new elitismo[]{elitismo.SI,elitismo.NO};	
		bloating[] bloat = new bloating[]{bloating.SI,bloating.NO};	
		contra[] contra = new contra[]{new SI(),new NO()};	
		
		enumEdades[] edades = new enumEdades[]{enumEdades.BILINEAL,enumEdades.LINEAL,enumEdades.PROPORCIONAL,enumEdades.NO};	
		enumSelecciones[] selecciones = new enumSelecciones[] {enumSelecciones.Ruleta,enumSelecciones.Torneo,enumSelecciones.Truncamiento,enumSelecciones.Restos,enumSelecciones.Ranking,enumSelecciones.Estocastico};
		enumMutaciones[] mutaciones = new enumMutaciones[] {enumMutaciones.TerminalSimple,enumMutaciones.FuncionalSimple,enumMutaciones.DeArbol};
		enumCruce[] cruce = new enumCruce[] { enumCruce.crucePorIntercambio};
		enumInicializaciones[] inicializacion = new enumInicializaciones[] {enumInicializaciones.Completa,enumInicializaciones.Creciente,enumInicializaciones.RampedAndHalf};
		
		
		ConfigPanel<algoritmoEvolutivo> config = new ConfigPanel<algoritmoEvolutivo>();
        CompoundBorder compound = BorderFactory.createCompoundBorder(
	    BorderFactory.createLineBorder(Color.LIGHT_GRAY),
	    BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		config.setBorder(compound);
		// se pueden añadir las opciones de forma independiente, o "de seguido"; el resultado es el mismo.
		config.addOption(new IntegerOption<algoritmoEvolutivo>(  // -- entero
				"Tamaño de la población", 					     // texto a usar como etiqueta del campo
				"Número de individuos de la población",       // texto a usar como 'tooltip' cuando pasas el puntero
				"tam_pob",  						     // campo (espera que haya un getTam_pob y un setTam_pob)
				1, Integer.MAX_VALUE))							     // min y max (usa Integer.MIN_VALUE /MAX_VALUE para infinitos)
			  .addOption(new IntegerOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Generaciones",							 // etiqueta 
			    "Número de generaciones", 					 // tooltip
			    "num_max_gen",   							 // campo (debe haber un getNum_max_gen y un setNum_max_gen)
			    1,Integer.MAX_VALUE))                            // elecciones posibles
			  .addOption(new DoubleOption<algoritmoEvolutivo>(   // -- doble, parecido a entero
			    "% cruce", 					 // etiqueta
			    "probabilidad para que se crucen los individuos",           // tooltip
			    "prob_cruce",                     // campo
			    0, 1,							     // min y max, aplicando factor, si hay; vale usar Double.*_INFINITY) 
			    1))								 // opcional: factor de multiplicacion != 1.0, para mostrar porcentajes
			  .addOption(new DoubleOption<algoritmoEvolutivo>( // -- eleccion de objeto configurable
				"% Mutación",							 // etiqueta
				"probabilidad de que los individuos muten",                // tooltip
				"prob_mut",                             // campo
				0,1,
				1)).addOption(new IntegerOption<algoritmoEvolutivo>(  // -- entero
				"Pasos", 					     // texto a usar como etiqueta del campo
				"Número de pasos que puede hacer la hormiga atómica en el tablero",       // texto a usar como 'tooltip' cuando pasas el puntero
				"pasos",  						     // campo (espera que haya un getTam_pob y un setTam_pob)
				400, Integer.MAX_VALUE)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Elitismo",							 // etiqueta 
			    "Si incluye elitismo o no", 		 // tooltip
			    "elit",   						 // campo (debe haber un getElitismo y un setElitismo)
			    elit)).addOption(new IntegerOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Profundidad",							 // etiqueta 
			    "Pronfundidad de los árboles generados", 					 // tooltip
			    "profundidad",   						 // campo (debe haber un getProfundidad y un setProfundidad)
			    1,20)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Selección",							 // etiqueta 
			    "Tipo de seleccion a usar", 		 // tooltip
			    "seleccion",   						 // campo (debe haber un getSeleccion y un setSeleccion)
			    selecciones)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Cruces",							 // etiqueta 
			    "Tipo de cruce", 		 // tooltip
			    "tipoCruce",   						 // campo (debe haber un getCruce y un setCruce)
			    cruce)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Mutaciones",							 // etiqueta 
			    "Tipo de mutacion", 		 // tooltip
			    "mutacion",   						 // campo (debe haber un getMutacion y un setMutacion)
			    mutaciones)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Incializacion",							 // etiqueta 
			    "Tipo de Inicializacion", 		 // tooltip
			    "incializacion",   						 // campo (debe haber un getIncializacion y un setIncializacion)
			    inicializacion)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
			    "Control de bloating",							 // etiqueta 
			    "Controlar cruce destructivo, priorizar individuos mas pequenios", 		 // tooltip
			    "bloat",   						 // campo (debe haber un getElitismo y un setElitismo)
			    bloat)).addOption(new ChoiceOption<algoritmoEvolutivo>(	 // -- eleccion de objeto no-configurable
  			    "Edades",							 // etiqueta 
			    "AG en lugar de métodos de selección, use cromosomas con edades", 		 // tooltip
			    "edad",   						 // campo (debe haber un getElitismo y un setElitismo)
			    edades)).addOption(new StrategyOption<algoritmoEvolutivo>( // -- eleccion de objeto configurable
				"Contractividad",							 // etiqueta
				"Aplicar contractividad al algoritmo",                // tooltip
				"contra",                             // campo
				contra)).beginInner(new InnerOption<algoritmoEvolutivo,contra>(
			    "Contractividad", "Aplicar contractividad al algoritmo", "contra", SI.class))
			  		  .addInner(new IntegerOption<algoritmoEvolutivo>(
			  		     "Máximo número de iteraciones", "cuantas veces como máximo va a iterar el algoritmo", "numMaxIter", 100, 1000))	  		  
			  		  .endInner()
			  
			  
			  
			  
			               
				
			 
		  		  
			  // y ahora ya cerramos el formulario
		  	  .endOptions();
		
		return config;
	}
	
	public static abstract class contra implements Cloneable {			
				
		// implementacion de 'clone' por defecto, suficiente para objetos sencillos
		public contra clone() { 
			try {
				return (contra)super.clone();
			} catch (CloneNotSupportedException e) {
				throw new IllegalArgumentException(e);
			} 
		}
	}
	
	/** un circulo (una forma, y por tanto 'cloneable') */
	public static class SI extends contra {
		private int numMaxIter = 500;

		public int getNumMaxIter() { return numMaxIter; }
		public void setNumMaxIter(int numMaxIter) { this.numMaxIter = numMaxIter;
		//System.out.println(numMaxIter); 
		}
		
		
		public String toString() {
			return "SI"; 
		}
		
		
	}	
	
	/** un rectangulo (una forma, y por tanto 'cloneable') */
	public static class NO extends contra {
		
		
		public String toString() {
			
			return "NO"; 
		}
	}	
	
public static void main(String[] args) {
		
		GUI p = new GUI();
		p.setSize(1400, 1100);
		p.setVisible(true);	
	}

}

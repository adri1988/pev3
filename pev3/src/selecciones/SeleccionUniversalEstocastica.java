package selecciones;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;


public class SeleccionUniversalEstocastica {

	public void ejecuta(ArrayList<cromosoma> pob) {
		int[] sel_super = new int[pob.size()];//seleccionados para sobrevivir
		double prob; // probabilidad de seleccion
		int pos_super; // posiciÃ³n del superviviente
		ArrayList<cromosoma> nueva_pob= new ArrayList<cromosoma>(pob.size()); 
		double r = 	Math.random();//elemento aleatorio único que voy a generar
		for (int i=1;i<pob.size();i++){//tengo que aplicar la formula Aj=(a+j-1)/k donde a es el valor aleatorio, j el j-ésimo elemento y la K tam_pob			
			pos_super=0;
			prob = 	Math.random();
			float Ai = (float) ((r+i-1)/pob.size());//calculo la posición del i-ésimo elemento 
			while ((prob>Ai) && (pos_super<pob.size())){
				sel_super[i] = pos_super;
				pos_super++;
			}
		}		
		for (int i=0;i<pob.size();i++){//genero la población intermedia
			nueva_pob.add( new cromosoma(pob.get(sel_super[i])));
		}
		pob = nueva_pob;//actualizo la población
		
	}

}

package algoritmoEvolutivo;

import operacionesArboles.*;
import utils.Funciones;
import utils.TArbol;
import utils.Terminales;

public class cromosoma {	
	   
TArbol arbol; // estrategia de rastreo
float aptitud;// función de evaluación
float puntuacion;//puntuacion relativa:adaptación/sumadaptacion
float punt_acu; // puntuacion acumulada
boolean elite; // elitismo 
private int hMaxima;
private  Terminales terminales;
private  Funciones funciones;
public evalua eval;
private creaArbolCreciente aCre;
private creaArbolCompleta aCom;
private creaArbolRampedAndHalf aRandH;
private int edad;
private boolean edadYaCalculada=false;





public cromosoma(int pasos){	
	eval = new evalua(pasos);
	aCre = new creaArbolCreciente();
	aCom = new creaArbolCompleta();
	aRandH = new creaArbolRampedAndHalf();
	arbol = new TArbol();
}
public cromosoma(cromosoma c){	
		
	this.aptitud=c.getAptitud();
	this.puntuacion=c.getPuntuacion();
	this.punt_acu=c.getPunt_acu();
	this.elite=c.isElite();
	this.hMaxima=c.gethMaxima();
	this.arbol=copiaArbol(c.arbol);
	this.aCom = c.aCom;
	this.aCre=c.aCre;
	this.aRandH=c.aRandH;
	this.eval=c.eval;
	
		
	
	
}

public int getEdad(){
	return edad;
}

public void setEdad(int edad){
	this.edad=edad;
}




private TArbol copiaArbol(TArbol arbol) {
	
	TArbol copia = clonaArbol (arbol);	
	
	return copia;
}
private TArbol clonaArbol(TArbol arbol) {
	
	TArbol p = new TArbol();

	if (!arbol.isEsHoja()){
	if (arbol.getHc()==null){
		p.setHi(clonaArbol(arbol.getHi()));
		p.setHd(clonaArbol(arbol.getHd()));
		
		
	}
	else {
		p.setHi(clonaArbol(arbol.getHi()));
		p.setHc(clonaArbol(arbol.getHc()));
		p.setHd(clonaArbol(arbol.getHd()));
		
		
	}
	}
	
	p.setValor(arbol.getValor());
	p.setEsHoja(arbol.isEsHoja());
	p.setNumHojas(arbol.getNumHojas());
	p.setNumNodos(arbol.getNumNodos());
	p.setEsHc(arbol.isEsHc());
	p.setEsHd(arbol.isEsHd());
	p.setEsHi(arbol.isEsHi());
	p.setPadre(arbol.getPadre());
	p.setProfundidad(arbol.getProfundidad());	
	return p;
}
public float evalua(int pasos) {
	eval = new evalua(pasos);
	return eval.aptitud(this);	
}


public void initCromosoma(int hMAx,int tipo) {	
	
	this.aptitud=0;
	this.punt_acu=0;
	this.hMaxima=hMAx;		
	/*
	 * tipo=0 completa
	 * tipo=1 creciente
	 * tipo=2 ramped and half
	 * */
	switch(tipo){
	case 0:
		aCom.arbolCompleta(this.arbol,0,hMaxima);
	
			
	
		
		break;
	case 1:
		aCre.arbolCreciente(this.arbol,0,hMaxima);
		
		break;
	case 2:
		//aRandH.arbolRampedHalf(this.arbol,hMaxima);//System.out.println("aRandH");		
		break;
	default: break;
	}
	
}




public TArbol getArbol() {
	return arbol;
}
public void setArbol(TArbol arbol) {
	this.arbol = arbol;
}
public float getAptitud() {
	return aptitud;
}
public void setAptitud(float aptitud) {
	this.aptitud = aptitud;
}
public float getPuntuacion() {
	return puntuacion;
}
public void setPuntuacion(float puntuacion) {
	this.puntuacion = puntuacion;
}
public float getPunt_acu() {
	return punt_acu;
}
public void setPunt_acu(float punt_acu) {
	this.punt_acu = punt_acu;
}
public boolean isElite() {
	return elite;
}
public void setElite(boolean elite) {
	this.elite = elite;
}
public int gethMaxima() {
	return hMaxima;
}
public void sethMaxima(int hMaxima) {
	this.hMaxima = hMaxima;
}
public Terminales getTerminales() {
	return terminales;
}
public void setTerminales(Terminales terminales) {
	this.terminales = terminales;
}
public Funciones getFunciones() {
	return funciones;
}
public void setFunciones(Funciones funciones) {
	this.funciones = funciones;
}
public boolean isEdadYaCalculada() {
	return edadYaCalculada;
}
public void setEdadYaCalculada(boolean edadYaCalculada) {
	this.edadYaCalculada = edadYaCalculada;
}

@Override
public String toString() {
	return "Aptitud :" + this.aptitud + " tamanio : "+ this.getArbol().getNumNodos();
		
}
public void eliminaRamasInutiles(int pasos) {

	
//	System.out.println(this.getAptitud()+ " " + this.getArbol().getNumNodos());
	utils.utils.eliminaRamasInutiles(this.getArbol(),pasos);
	utils.utils.actualizaArbol(this.getArbol(), 0);
	this.evalua(pasos);
//	System.out.println(this.getAptitud()+ " " + this.getArbol().getNumNodos() );
//	System.out.println("----------------------------------------");
	
}
public void modificaArbolAmiGusto() {
	creaArbolCompleta a = new creaArbolCompleta();
	TArbol aux = new TArbol();
	TArbol auxI = new TArbol();
	TArbol auxD = new TArbol();
	
	
	auxI.setValor("SIC");
	auxI.setNumNodos(3);
	auxD.setValor("PROGN2");
	auxI.setNumNodos(3);
	
	
	
	
	TArbol hi = new TArbol();
	TArbol hi2 = new TArbol();
	TArbol hd = new TArbol();
	TArbol hd2 = new TArbol();
	
	a.arbolCompleta(hi, 0, 0);
	a.arbolCompleta(hi2, 0, 0);
	a.arbolCompleta(hd, 0, 0);
	a.arbolCompleta(hd2, 0, 0);
	
	auxI.setHi(hi);
	auxI.setHd(hd);
	auxI.setHc(null);
	
	auxD.setHi(hi2);
	auxD.setHd(hd2);
	auxD.setHc(null);
	
	
	aux.setHi(auxI);
	aux.setHc(null);
	aux.setHd(auxD);
	aux.setValor("PROGN2");
	aux.setNumNodos(4);	
	arbol = aux;
	int fit = (int) this.evalua(400);
	
	
	
}






}

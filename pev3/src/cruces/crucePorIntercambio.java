package cruces;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;
import utils.TArbol;
import utils.utils;

public class crucePorIntercambio {

	

	public void ejecuta(cromosoma padre1, cromosoma padre2, cromosoma hijo1, cromosoma hijo2,int pasos) {
		
		 TArbol subarbol1 = null, subarbol2 = null;
		 int num_nodos;
		 utils.actualizaArbol(hijo1.getArbol(), 0);
		 utils.actualizaArbol(hijo2.getArbol(), 0);
		 utils.actualizaArbol(padre1.getArbol(), 0);
		 utils.actualizaArbol(padre2.getArbol(), 0);
		 num_nodos=Math.min(padre1.getArbol().getNumNodos(), padre2.getArbol().getNumNodos());//(num_nodos(padre1.arbol),num_nodos(padre2.arbol));
		 int nodo_cruce = utils.randomWithRange(0,num_nodos-1);
		 
		double prob_Funcion = Math.random();
		
		
		if (prob_Funcion<0.9){
			if (!hijo1.getArbol().isEsHoja()){
				
				subarbol1 = generaArbolNoHoja(hijo1);
			}
			else{
			
				subarbol1 = generaArbolHoja(hijo1);
			}
			
			if (!hijo2.getArbol().isEsHoja()){
		
				subarbol2 = generaArbolNoHoja(hijo2);	
			}
			else{
			
				subarbol2 = generaArbolHoja(hijo2);	
			}
			
		}
		else{
			
		
			subarbol1 = generaArbolHoja(hijo1);
			
			
			
			subarbol2 = generaArbolHoja(hijo2);	
		
		}
		
	 
		utils.sustituirSubarbol( hijo1.getArbol(), nodo_cruce, subarbol2);		
	    utils.sustituirSubarbol( hijo2.getArbol(), nodo_cruce, subarbol1);		
	  
		hijo1.evalua(pasos);
		hijo2.evalua(pasos);
		
	}

	private TArbol generaArbolHoja(cromosoma hijo1) {
		
		TArbol subarbol1 = null;
		int nodo_cruce = utils.randomWithRange(0,hijo1.getArbol().getNumNodos()-1);
		subarbol1 = utils.copiaArbol(hijo1.getArbol().BuscarNodo(nodo_cruce)); 
		int contador =0;
		
		while (!subarbol1.isEsHoja()){//voy obteniendo subarboles hasta q salga uno q no sea hoja
			 nodo_cruce = utils.randomWithRange(0,hijo1.getArbol().getNumNodos()-1);
			 subarbol1 = utils.copiaArbol(hijo1.getArbol().BuscarNodo(nodo_cruce));
			 contador++;
			 if (contador>=100){
				 System.out.println("estoy en el bucle infinito");
			 }
		}
		
		return subarbol1;
	}

	private TArbol generaArbolNoHoja(cromosoma hijo1 ) {
		 
		TArbol subarbol1 = null;
		int nodo_cruce = utils.randomWithRange(0,hijo1.getArbol().getNumNodos()-1);
		subarbol1 = utils.copiaArbol(hijo1.getArbol().BuscarNodo(nodo_cruce)); 
		
		while (subarbol1.isEsHoja()){//voy obteniendo subarboles hasta q salga uno q no sea hoja
			 nodo_cruce = utils.randomWithRange(0,hijo1.getArbol().getNumNodos()-1);
			 subarbol1 = utils.copiaArbol(hijo1.getArbol().BuscarNodo(nodo_cruce));
		}
		
		return subarbol1;
	}

	public void ejecutaSoloUnHijo(cromosoma padre1,cromosoma padre2, cromosoma hijo, int pasos,int queHijoEs) {
		 TArbol subarbol = null;
		 int num_nodos;
		 utils.actualizaArbol(hijo.getArbol(), 0);	
		
		 cromosoma padre=null;
		 if (queHijoEs==1)
			 padre = new cromosoma(padre2);
		 else
			 padre = new cromosoma(padre1);
		 num_nodos=Math.min(padre1.getArbol().getNumNodos(), padre2.getArbol().getNumNodos());//(num_nodos(padre1.arbol),num_nodos(padre2.arbol));		
		 int nodo_cruce = utils.randomWithRange(0,num_nodos-1);
		 
		double prob_Funcion = Math.random();
		
	//	System.out.println("Entro bien");
		if (prob_Funcion<0.9){
		//	System.out.println("soy menor q 0.9 entrando");
			
			if (!padre.getArbol().isEsHoja()){
			//	System.out.println("No soy hoja");
			//	System.out.println(padre.getArbol());
				subarbol = generaArbolNoHoja(padre);
			}
			else{	//System.out.println("soy hoja");
			
				subarbol = generaArbolHoja(padre);
			}
			
			//System.out.println("soy menor q 0.9 saliendo");

			
		}
		else{
			
		
			subarbol = generaArbolHoja(padre);	
			
			
		
		}
		
		//System.out.println("salgo bien");
		
	 
		utils.sustituirSubarbol( hijo.getArbol(), nodo_cruce, subarbol);		
		
	  
		hijo.evalua(pasos);
		
		
	}
	
	
	
	
}

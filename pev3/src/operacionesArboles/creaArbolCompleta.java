package operacionesArboles;

import utils.Funciones;
import utils.TArbol;
import utils.Terminales;
import utils.utils;

public class creaArbolCompleta {
	
	public void arbolCompleta(TArbol arbol2, int hMAx,int hMaxima) {	
		
		if (hMAx < hMaxima ){
			String operador = utils.operador_aleatorio();	
			arbol2.setValor(operador);
			if (operador.equals(Funciones.PROGN3.toString())){
				int numNodos=0;
				int numHojas=0;
				arbol2.setHi(new TArbol());
				arbol2.setHc(new TArbol());
				arbol2.setHd(new TArbol());
				
				
				arbolCompleta(arbol2.getHi(),hMAx+1,hMaxima);				
				setValoresArbol(arbol2,0,true,hMAx);	
				
				arbolCompleta(arbol2.getHc(),hMAx+1,hMaxima);			
				setValoresArbol(arbol2,1,true,hMAx);
				
				arbolCompleta(arbol2.getHd(),hMAx+1,hMaxima);				
				setValoresArbol(arbol2,2,true,hMAx);
				
				
				
				
				numNodos = arbol2.getHi().getNumNodos() +  arbol2.getHd().getNumNodos() +  arbol2.getHc().getNumNodos()+1;
				numHojas = arbol2.getHi().getNumHojas() +  arbol2.getHd().getNumHojas() + arbol2.getHc().getNumHojas();
				arbol2.setNumNodos(numNodos);				
				arbol2.setNumHojas(numHojas);
				
			}
			else{
				int numNodos=0;	
				int numHojas=0;
				arbol2.setHi(new TArbol());
				arbol2.setHd(new TArbol());
				
				arbolCompleta(arbol2.getHi(),hMAx+1,hMaxima);			
				setValoresArbol(arbol2,0,true,hMAx);
				
				arbolCompleta(arbol2.getHd(),hMAx+1,hMaxima);
				setValoresArbol(arbol2,2,true,hMAx);
				numNodos = arbol2.getHi().getNumNodos() +  arbol2.getHd().getNumNodos()+1;							
				arbol2.setNumNodos(numNodos);
				
				numHojas = arbol2.getHi().getNumHojas() +  arbol2.getHd().getNumHojas();	
				arbol2.setNumHojas(numHojas);
				
				arbol2.setHc(null);
			}
			
			
			
		}
		else{
		double num =Math.random();
		if (num<0.8){
			
			arbol2.setValor("AVANZA");
			arbol2.setNumNodos(1);	
			arbol2.setEsHoja(true);
			arbol2.setNumHojas(1);
			arbol2.setProfundidad(hMAx);
		}
		else{
			String operando = utils.operando_aleatorio();
			arbol2.setValor(operando);
			arbol2.setNumNodos(1);	
			arbol2.setEsHoja(true);
			arbol2.setNumHojas(1);
			arbol2.setProfundidad(hMAx);
		}
		}
		
	}
	
	private void setValoresArbol(TArbol arbol2, int hijoX, boolean b, int hMAx) {
		/*
		 * hijoX == 0 esHi
		 * hijoX == 1 esHc
		 * hijoX == 2 esHd
		 * */
		arbol2.setProfundidad(hMAx);
		
		switch (hijoX){
		case 0: arbol2.getHi().setEsHi(true);
				arbol2.getHi().setPadre(arbol2);
		break;
		case 1: arbol2.getHc().setEsHc(true);
				arbol2.getHc().setPadre(arbol2);
		break;
		case 2: arbol2.getHd().setEsHd(true);
				arbol2.getHd().setPadre(arbol2);
		break;
		default: break;
			
		}
		
	}

	

}

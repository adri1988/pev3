package operacionesArboles;

import java.util.ArrayList;

import algoritmoEvolutivo.cromosoma;


public class creaArbolRampedAndHalf {
	
	
	public void arbolRampedHalf(ArrayList<cromosoma> pob, int max,int tam_pob,int pasos) {// la mitad serán crecientes la otra mitad rampedAndHalf
	
		boolean creciente = false;//el primer grupo va a ser de árboles completos, el segundo crecientes etc etc
		int individuosPorGrupo = tam_pob / (max-1);
		int contador=0;
		int individuosCreados=0;//mantenemos un contador de individuos creados, ya que la division de tam_pob/grupos-1 puede no ser exacta
		int alturaGrupo=2;
		while (individuosCreados<tam_pob){
			int grupoNuevo=0;
			
			while (grupoNuevo<individuosPorGrupo && individuosCreados<tam_pob){//bucle para crear el grupo de individuos, asegurandonos de que no nos pasamos de tam_pob
				pob.add(new cromosoma(pasos));
				if (creciente)
					pob.get(individuosCreados).initCromosoma(alturaGrupo,1);
				else
					pob.get(individuosCreados).initCromosoma(alturaGrupo,0);
				
				pob.get(individuosCreados).evalua(pasos);
				individuosCreados++;
				grupoNuevo++;
				
			}
			alturaGrupo++;
			if (creciente)
				creciente=false;
			else
				creciente=true;
			
		}
		
	}

}

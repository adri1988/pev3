package algoritmoEvolutivo;

import java.util.ArrayList;

import utils.Funciones;
import utils.TArbol;
import utils.utils;

public class evalua {	
	
	String[][] mapaSantaFe;
	 
	
	int bocados=0;
	int pasos=0;
	int pasosLimite=400;
	int posicionX=0;
	int posicionY=0;
	int direccion =1;
	private int limite = 32;
	public ArrayList<String> movimientos;
	/*
		0-norte
		1-este
		2-sur
		3 oeste
	*/
	boolean[][] recorridoHormiga;
	String comidaH="#";
	
	public evalua(int pasos){
		this.pasosLimite=pasos;
		
	}
	
	public int aptitud(cromosoma individuo){
		posicionX=0;
		posicionY=0;
		bocados=0;
		pasos=0;
		initMapaSantaFe();
		//System.out.println("pasos: " + pasosLimite);		
		ejecutaArbol(individuo.arbol);	
		individuo.setAptitud(bocados);
	
		
		return bocados;
		
	}
	

	

	private void ejecutaArbol(TArbol arbol) {
		
		if (pasos<pasosLimite && bocados<88){	
			//ahora vamos a ver el nodo en el que estamos y realizamos acción
			
			switch (arbol.getValor()){
				case "PROGN3":
					ejecutaArbol(arbol.getHi());
					ejecutaArbol(arbol.getHc());
					ejecutaArbol(arbol.getHd());					
					break;
				case "PROGN2":
					ejecutaArbol(arbol.getHi());
					ejecutaArbol(arbol.getHd());
					break;
				case "SIC":
					if (hayComida()){						
						ejecutaArbol(arbol.getHi());
					}
					else
						ejecutaArbol(arbol.getHd());	
					break;
				case "AVANZA":
					avanza();
					if (mapaSantaFe[posicionY][posicionX].equals(comidaH)){
						mapaSantaFe[posicionY][posicionX] = "1";//el 1 significa que he comido en esa posición
						bocados++;
					}
					pasos++;
					break;
				case "GIRA_DERECHA":
					derecha();
					pasos++;
					break;
				case "GIRA_IZQUIERDA":
					izquierda();
					pasos++;
					break;
				default:break;				
					
			}
			
		}
		//if (pasos >= pasosLimite)
		//	System.out.println("Pasos: "+pasos + "  y bocados: "+ bocados );
	//	if (bocados>=88)
		//	System.out.println("game over");
		
	}
	
	
	/*
	0-norte
	1-este
	2-sur
	3 oeste
*/

	private void izquierda() {
	//	System.out.print("GIRA_IZQUIERDA,");
		movimientos.add("GIRA_IZQUIERDA");
		switch (direccion){
		case 0:
			direccion=3;
			break;
		case 1:
			direccion=0;
			break;
		case 2:
			direccion=1;
			break;
		case 3:
			direccion=2;
			break;
		}
		
	}

	private void derecha() {
		//System.out.print("GIRA_DERECHA,");
		movimientos.add("GIRA_DERECHA");
		switch (direccion){
		case 0:
			direccion=1;
			break;
		case 1:
			direccion=2;
			break;
		case 2:
			direccion=3;
			break;
		case 3:
			direccion=0;
			break;
		}
		
	}
	
	/*
	0-norte
	1-este
	2-sur
	3 oeste
*/

	private void avanza() {
		
		switch (direccion){
		case 0:
		
			//	System.out.print("AVANZA,");
				movimientos.add("AVANZA");
				if (posicionY==0)
					posicionY=31;
				else
					posicionY--;
		
				
		
			break;
		case 1:
			    if (posicionX==31)
			    	posicionX=0;
			    else
			    	posicionX++;
			//	System.out.print("AVANZA,");
				movimientos.add("AVANZA");
			
				
			break;
		case 2:
	            if (posicionY==31)
	            	posicionY=0;
	            else	            	
	            	posicionY++;
				//System.out.print("AVANZA,");
				movimientos.add("AVANZA");
			
			
			break;
		case 3:
			     if (posicionX==0)
			    	 posicionX=31;
			     else
			    	 posicionX--;
			//	System.out.print("AVANZA,");
				movimientos.add("AVANZA");
			
			
			break;
		}
		
		
		//mapaSantaFe[posicionX][posicionY]="1";
		
		
	}

	private boolean sePuedeAvanzar() {
		boolean resultado=true;
		switch (direccion){
		case 0:		
			if (posicionX==0)
				resultado=false;
			break;
		case 1:			
			if (posicionY==31)
				resultado=false;
			break;
		case 2:
			if (posicionX==31)
				resultado=false;
			break;
		case 3:
			if (posicionY==0)
				resultado=false;
			break;
		}
		
		
		return resultado;
	}

	private boolean hayComida() {
		
			
		try{
		
		boolean comida=false;
			switch (direccion){
			case 0:
				if (posicionY==0){				
					if (mapaSantaFe[31][posicionX].equals(comidaH))
						comida=true;
				}
				else{
					if (mapaSantaFe[posicionY-1][posicionX].equals(comidaH))
						comida=true;
				}
				
					
				break;
			case 1:
					if (posicionX==31){
						if (mapaSantaFe[posicionY][0].equals(comidaH))
							comida=true;
					}
					else{
						if (mapaSantaFe[posicionY][posicionX+1].equals(comidaH))
							comida=true;
					}
						
				break;
			case 2:
					if (posicionY ==31){
						if (mapaSantaFe[0][posicionX].equals(comidaH))
							comida=true;
					}
					else{
						if (mapaSantaFe[posicionY+1][posicionX].equals(comidaH))
							comida=true;
					}
					
				break;
			case 3:
					if (posicionX==0){
						if (mapaSantaFe[posicionY][31].equals(comidaH))
							comida=true;
					}
					else{
						if (mapaSantaFe[posicionY][posicionX-1].equals(comidaH))
							comida=true;
					}
				
					
				break;
				default : break;
			}
			
			return comida;
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
		return false;
		
	}
	
	@SuppressWarnings("static-access")
	private void initMapaSantaFe() {
		mapaSantaFe = new String[this.limite][this.limite];		
		movimientos = new ArrayList<String>();
		for (int i=0;i<this.limite;i++){
			for (int j=0;j<this.limite;j++){
				if (utils.MAPASANTAFE[i][j].equals("1"))
						utils.MAPASANTAFE[i][j]="#";	
				mapaSantaFe[i][j]=utils.MAPASANTAFE[i][j];
			}
		}
		
		verificaSiHayUnos(mapaSantaFe);
			
			
		
	}

	private void verificaSiHayUnos(String[][] mapaSantaFe2) {
		
		for (int i=0;i<this.limite;i++){
			for (int j=0;j<this.limite;j++){
				if (mapaSantaFe2[i][j].equals("1"))
					System.out.println("Hay uno");
			}
		}
		
	}
	
	
	
	

}